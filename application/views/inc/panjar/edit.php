<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('layout/header') ?>
    </head>
    <body>
        <!-- Navigation Bar-->
        <?php $this->load->view('layout/navigation') ?>
        <!-- End Navigation Bar-->
        
        <!-- wrapper -->
        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                    <li class="breadcrumb-item active"> <?= $title ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <!-- Main -->
                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <h5>Halaman <?= $title ?></h5>
                            <br>
                            
                            <?php echo form_open_multipart($controller.'/update'); ?>
                                <fieldset>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="col-form-label">Nama Alat</label>
                                            <input type="text" class="form-control" required name="name" value="<?= $list_edit->name ?>">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-form-label">Kondisi</label>
                                            <select class="form-control" required name="kondisi">
                                                <?php foreach ($kondisi as $key => $k): ?>
                                                    <option value="<?= $key ?>" <?= $key == $list_edit->kondisi ? 'selected' : '' ?> > <?= $k ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="col-form-label">KKS</label>
                                            <input type="text" class="form-control" required name="kks" value="<?= $list_edit->kks ?>">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-form-label">Unit</label>
                                            <select class="form-control" required name="id_department">
                                                <?php foreach ($department as $key => $d): ?>
                                                    <option value="<?= $d->id ?>" <?= $d->id == $list_edit->id_department ? 'selected' : '' ?> > <?= ucwords($d->name) ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="col-form-label">User</label>
                                            <select class="form-control" required name="id_jenisuser">
                                                <?php foreach ($jenisuser as $key => $j): ?>
                                                    <option value="<?= $j->id ?>" <?= $j->id == $list_edit->id_jenisuser ? 'selected' : '' ?> > <?= ucwords($j->name) ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-form-label">Distribution</label>
                                            <select class="form-control" required name="id_distribution">
                                                <?php foreach ($distribution as $key => $di): ?>
                                                    <option value="<?= $di->id ?>" <?= $di->id == $list_edit->id_distribution ? 'selected' : '' ?> > <?= ucwords($di->name) ?> 
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="col-form-label">Tanggal Operasin</label>
                                            <input type="date" class="form-control" required name="date_operation" value="<?= date('Y-m-d',strtotime($list_edit->date_operation)) ?>">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-form-label">Jam Operasi</label>
                                            <input type="text" class="form-control" required name="hour_operation" placeholder="120:53:23" value="<?= $list_edit->hour_operation ?>">
                                            <small>pisahkan dengan <b><u>: (titik 2)</u></b> , untuk membedakan jam:menit:detik</small>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="col-form-label">Financial</label>
                                            <input type="number" class="form-control" required name="financial" value="<?= $list_edit->financial ?>">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-form-label">Operasi</label>
                                            <input type="number" class="form-control" required name="operation" value="<?= $list_edit->operation ?>">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="col-form-label">Lingkungan</label>
                                            <input type="number" class="form-control" required name="environment" value="<?= $list_edit->environment ?>">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-form-label">Safety</label>
                                            <input type="number" class="form-control" required name="safety" value="<?= $list_edit->safety ?>">
                                        </div>
                                    </div>
                                </fieldset>

                                <div>
                                    <br>
                                    <input type="hidden" name="id" value="<?= $list_edit->id ?>">
                                    <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <!-- End Main -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- Footer -->
        <?php $this->load->view('layout/footer') ?>
        <!-- End Footer -->

    </body>
</html>