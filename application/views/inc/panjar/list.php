<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('layout/header') ?>
    </head>
    <body>
        <!-- Navigation Bar-->
        <?php $this->load->view('layout/navigation') ?>
        <!-- End Navigation Bar-->
        
        <!-- wrapper -->
        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                    <li class="breadcrumb-item active"> <?= $title ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <!-- Main -->
                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <h5> Data <?= $title ?></h5>
                            <br>
                            <?php echo form_open_multipart($controller); ?>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select class="form-control" name="bulan">
                                                <?php for($i=1;$i<=12;$i++) { ?>
                                                    <option value="<?= $i ?>" <?= !empty($filter) ? ($filter['bulan'] == $i ? 'selected' : '') : '' ; ?> > <?= date('F',strtotime('2018-'.$i.'-01')) ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select class="form-control" name="tahun">
                                            <?php for($i=1;$i<=10;$i++) { ?>
                                                <option value="<?= $i+2015 ?>" <?= !empty($filter) ? ($filter['tahun'] == $i+'2015' ? 'selected' : '') : '' ; ?>> <?= $i+2015 ?> </option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <button type="submit" class="btn btn-info waves-light waves-effect w-md show">Tampilkan</button>
                                    </div>
                                </div>
                            </form>
                            <button type="button" class="btn btn-success waves-light waves-effect w-md" data-toggle="modal" data-target=".create">Tambah</button>
                            
                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Kode Kegiatan</th>
                                        <th width="20%">Kegiatan</th>
                                        <th>Department</th>
                                        <th>Panjar / Dibuat</th>
                                        <th>Kas</th>
                                        <!-- <th>NTB (Rp.)</th>
                                        <th>NTPN (Rp.)</th> -->
                                        <th>Total (Rp. )</th>
                                        <th width="15%">Action</th>
                                        <th>Cetak</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $i = 1 ;
                                    foreach ($list as $key =>$row) { ?>
                                        <tr>
                                            <td><?= $i++ ?></td>
                                            <td><?= ucwords($row->name) ?></td>
                                            <td><?= strtoupper($row->kode) ?></td>
                                            <td><?= ucwords($row->description) ?></td>
                                            <td><?= strtoupper($row->id_department) ?></td>
                                            <td><?= date('d M Y', strtotime($row->tanggal)) .' / '. date('d M Y', strtotime($row->date_created))?></td>
                                            <td><?= $row->jenis == '1' ? 'GU' : 'TU' ?></td>
                                            <!-- <td><b><?= number_format($row->ntb) ?></b></td>
                                            <td><b><?= number_format($row->ntpn) ?></b></td> -->
                                            <td><b><?= number_format($row->total) ?></b></td>
                                            <td align="center">
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-warning btn-xs edited" data-id="<?= $row->id; ?>" > <i class="fa fa-wrench"></i> </button>
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-danger btn-xs delete" data-id="<?= $row->id; ?>"> <i class="fa fa-remove"></i> </button>
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-success btn-xs detail" data-id="<?= $row->id; ?>"> <i class="fa fa-clipboard"></i> </button>
                                            </td>
                                            <td align="center">
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-purple  btn-xs laporan" data-id="<?= $row->id; ?>"> </i> Laporan</button>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End Main -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- Modal -->
        <div class="modal fade bs-example-modal-lg create" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Input Data Panjar</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/insert'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input class="form-control" type="text" required name="name">
                                </div>

                                <div class="form-group">
                                    <label>Kegiatan</label>
                                    <select class="form-control" required name="id_kegiatan">
                                        <?php foreach ($kegiatan as $key => $k): ?>
                                            <option value="<?= $k->id ?>" > 
                                                <?= $k->kode.' - '.strtoupper(substr($k->description,0,70)); ?> 
                                                <?= strlen($k->description ) > 70 ? '....' : ''; ?>
                                            </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Tanggal Panjar</label>
                                    <input class="form-control" type="date" required name="tanggal">
                                </div>

                                <div class="form-group">
                                    <label>GU/TU</label>
                                    <select class="form-control" required name="jenis">
                                        <option value="1">GU</option>
                                        <option value="2">TU</option>
                                    </select>
                                </div>
                            </fieldset>

                            <div>
                                <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <div class="modal fade bs-example-modal-lg edit-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Penambahan Data</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/update'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input class="form-control" type="text" required name="name" id="name">
                                </div>

                                <div class="form-group">
                                    <label>Kegiatan</label>
                                    <select class="form-control" readonly="true" disabled name="id_kegiatan" id="id_department">
                                        <?php foreach ($kegiatan as $key => $k): ?>
                                            <option value="<?= $k->id ?>" > 
                                                <?= $k->kode.' - '.strtoupper(substr($k->description,0,70)); ?> 
                                                <?= strlen($k->description ) > 70 ? '....' : ''; ?>
                                            </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Tanggal Panjar</label>
                                    <input class="form-control" type="date" required name="tanggal" id="tanggal">
                                </div>
                                <div class="form-group">
                                    <label>GU/TU</label>
                                    <select class="form-control" required name="jenis" id="jenis">
                                        <option value="1">GU</option>
                                        <option value="2">TU</option>
                                    </select>
                                </div>
                            </fieldset>

                            <div>
                                <input type="hidden" name="id" id="id">
                                <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <!-- Modal -->

        <!-- Footer -->
        <?php $this->load->view('layout/footer') ?>
        <!-- End Footer -->

        <script type="text/javascript">
            var url = "<?= site_url().$controller; ?>";

            $('.edited').click(function(){
                $.ajax({
                    url: url + '/edit/' + $(this).data('id'),
                    type:'GET',
                    dataType: 'json',
                    success: function(data){
                        
                        $("#id").val(data['list_edit']['id']);
                        $("#id_kegiatan").val(data['list_edit']['id_kegiatan']);                                    
                        $("#name").val(data['list_edit']['name']);                                    
                        $("#tanggal").val(data['list_edit']['tanggal']);
                        $("#jenis").val(data['list_edit']['jenis']);                                                                       

                        $(".edit-modal").modal('show');
                    }                
                }); 
            })

            $('.delete').click(function () {
                var id = $(this).data('id') ;
                swal({
                    title: 'Apakah Data ini ingin di Hapus? ',
                    showCancelButton: true,
                    confirmButtonColor: '#4fa7f3',
                    cancelButtonColor: '#d57171',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    window.location = url + '/delete/' + id ;
                })
            });

            $('.detail').click(function(e){
                e.preventDefault();
                window.location = url + '/detail/' + $(this).data('id') ;
            })

            $('.laporan').click(function(e){
                e.preventDefault();
                window.location = url + '/laporan/' + $(this).data('id') ;
            })
        </script>
    </body>
</html>