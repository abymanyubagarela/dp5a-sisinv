<!DOCTYPE html>

<html>
    <head>
        <?php $this->load->view('layout/header') ?>
    </head>
    <body>
        <!-- Navigation Bar-->
        <?php $this->load->view('layout/navigation') ?>
        <!-- End Navigation Bar-->
        
        <!-- wrapper -->
        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                    <li class="breadcrumb-item active"> <?= $title ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <!-- Main -->
                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <h5>Halaman <?= $title ?></h5>
                            <br>

                            <?php if (!empty($list_edit)): ?>                            
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <tr>
                                    <td width="10%">Nama Kegiatan</td>
                                    <th width="30%"><u><?= ucwords($list_edit->name) ?></u></th>
                                    <td width="10%">Tanggal</td>
                                    <th width="30%"><u><?= date('d M Y',strtotime($list_edit->tanggal)) ?></u></th>
                                </tr>

                                <tr>
                                    <td width="10%">Kegiatan</td>
                                    <th width="30%" colspan="3"><u><?= ucfirst($list_edit->kode.' - '.$list_edit->description) ?></u></th>
                                </tr>

                                <tr>
                                    <td width="10%">Department</td>
                                    <th width="30%" colspan="3"><u><?= strtoupper($list_edit->id_department) ?></u></th>
                                </tr>
                            </table>
                            <?php else: ?>
                            <p>Data tidak ditemukan</p>
                            <?php endif ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <button type="button" class="btn btn-success waves-light waves-effect w-md" data-toggle="modal" data-target=".create">Tambah</button>
                            
                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Rekening</th>
                                        <th>Deskripsi</th>
                                        <th>Nilai Panjar</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $i = 1 ;

                                    foreach ($list as $key =>$row) { ?>
                                        <tr>
                                            <td><?= $i++ ?></td>
                                            <td><?= strtoupper($row->kode) ?></td>
                                            <td><?= ucwords($row->description) ?></td>
                                            <td>Rp. <?= number_format($row->nominal) ?></td>
                                            <td align="center">
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-warning btn-xs edited" data-id="<?= $row->id; ?>" > <i class="fa fa-wrench"></i> </button>
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-danger btn-xs delete" data-id="<?= $row->id; ?>"> <i class="fa fa-remove"></i> </button>
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-success btn-xs detail" data-id="<?= $row->id_kegiatan.'-'.$row->id_rekening; ?>"> <i class="fa fa-clipboard"></i> </button>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End Main -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- Modal -->
        <div class="modal fade bs-example-modal-lg create" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Tambahan Data</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/insertdetail'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>Rekening</label>
                                    <select class="form-control"required name="id_rekening">
                                        <?php foreach ($rekening as $key => $k): ?>
                                            <option value="<?= $k->id ?>" > 
                                                <?= $k->kode.' - '.strtoupper(substr($k->description,0,70)); ?> 
                                                <?= strlen($k->description ) > 70 ? '....' : ''; ?>
                                            </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Panjar (Rp)</label>
                                      <input class="form-control" type="number" required name="nominal">
                                </div>
                            </fieldset>

                            <div>
                                <input type="hidden" name="id_kegiatan" value="<?= $list_edit->id ?>">
                                <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <!-- Modal -->
        <div class="modal fade bs-example-modal-lg edit-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Penambahan Data</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/updatedetail'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>Rekening</label>
                                    <select class="form-control"required name="id_rekening" id="id_rekening">
                                        <?php foreach ($rekening as $key => $k): ?>
                                            <option value="<?= $k->id ?>" > 
                                                <?= $k->kode.' - '.strtoupper(substr($k->description,0,70)); ?> 
                                                <?= strlen($k->description ) > 70 ? '....' : ''; ?>
                                            </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Panjar (Rp)</label>
                                    <input class="form-control" type="number" required name="nominal" id="nominal">
                                </div>
                            </fieldset>

                            <div>
                                <input type="hidden" name="id" id="id">
                                <input type="hidden" name="id_kegiatan" id="id_kegiatan">
                                <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <!-- Modal -->

        <!-- Footer -->
        <?php $this->load->view('layout/footer') ?>
        <!-- End Footer -->
       
        <script type="text/javascript">
            var url = "<?= site_url().$controller; ?>";

            $('.edited').click(function(){
                $.ajax({
                    url: url + '/editdetail/' + $(this).data('id'),
                    type:'GET',
                    dataType: 'json',
                    success: function(data){
                        
                        $("#id").val(data['list_edit']['id']);
                        $("#id_kegiatan").val(data['list_edit']['id_kegiatan']);                                    
                        $("#id_rekening").val(data['list_edit']['id_rekening']);                                    
                        $("#nominal").val(data['list_edit']['nominal']);                                  

                        $(".edit-modal").modal('show');
                    }                
                }); 
            })

            $('.delete').click(function () {
                var id = $(this).data('id') ;

                swal({
                    title: 'Apakah Data ini ingin di Hapus? ',
                    showCancelButton: true,
                    confirmButtonColor: '#4fa7f3',
                    cancelButtonColor: '#d57171',
                    confirmButtonText: 'Ya , Hapus data!',
                    cancelButtonText: 'Hapus'
                }).then(function () {
                    $.ajax({
                        url: url + '/deletedetail/' + id,
                        type:'POST',
                        dataType: 'json',
                        success: function(data){
                            location.reload();    
                        }                
                    }); 
                })
            });

            $('.detail').click(function(e) {
                var url = "<?= site_url(); ?>";
                
                e.preventDefault();
                window.location = url + 'detailrekening/detail/' + $(this).data('id') ;
            })
        </script>
    </body>
</html>