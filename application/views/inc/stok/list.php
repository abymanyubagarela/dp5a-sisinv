<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('layout/header') ?>
    </head>
    <body>
        <!-- Navigation Bar-->
        <?php $this->load->view('layout/navigation') ?>
        <!-- End Navigation Bar-->
        
        <!-- wrapper -->
        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                    <li class="breadcrumb-item active"> <?= $title ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <!-- Main -->
                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <h5> Data <?= $title ?></h5>
                            <?php if ($jenis == '1'): ?>
                            <button type="button" class="btn btn-success waves-light waves-effect w-md" data-toggle="modal" data-target=".create">Tambah</button>    
                            <?php endif ?>
                            
                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Barang</th>
                                        <th>Jumlah</th>
                                        <th>Keterangan</th>
                                        <th>Tanggal <?= $jenis == '1' ? 'Masuk' : 'Keluar' ?> </th>
                                        <th>Tanggal Input</th>
                                        <?php if ($jenis == '1'): ?>
                                        <th>Action</th>
                                        <?php endif ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $i = 1 ;
                                    foreach ($list as $key =>$row) { ?>
                                        <tr>
                                            <td><?= $i++ ?></td>
                                            <td><?= $row->id_komponen.' - '.$row->name ?></td>
                                            <td><?= $row->jumlah ?></td>
                                            <td><?= $row->note ?></td>
                                            <td><?= date("d M Y", strtotime($row->tanggal_masuk)); ?></td>
                                            <td><?= date("d M Y", strtotime($row->date_created)); ?></td>                                            
                                            <?php if ($jenis == '1'): ?>
                                            <td align="center">
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-warning btn-xs edit" data-id="<?= $row->id; ?>" > <i class="fa fa-wrench"></i> </button>
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-danger btn-xs delete" data-id="<?= $row->id; ?>"> <i class="fa fa-remove"></i> </button>
                                            </td>
                                            <?php endif ?>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End Main -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- Modal -->
        <div class="modal fade bs-example-modal-lg create" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Penambahan Data</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/insert'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>Nama Barang</label>
                                    <select class="form-control select2" name="id_barang" required>
                                        <?php foreach ($barang as $key => $value): ?>
                                        <option value="<?= $value->id ?>" > <?= $value->id_komponen.' - '.$value->name ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Jumlah</label>
                                    <input class="form-control" type="number" required name="jumlah">
                                </div>
                                <div class="form-group">
                                    <label>Tanggal <?= $jenis == '1' ? 'Masuk' : 'Keluar' ?></label>
                                    <input class="form-control" type="date" required name="tanggal_masuk">
                                </div>
                                <div class="form-group">
                                    <label> Keterangan</label>
                                    <textarea class="form-control" type="text" name="note"></textarea>
                                </div>
                            </fieldset>

                            <div>
                                <input type="hidden" name="jenis" value="<?= $jenis ?>">
                                <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <div class="modal fade bs-example-modal-lg edit-modal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Penambahan Data</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/update'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>Nama Barang</label>
                                    <select class="form-control select2" id="id_barang" required disabled>
                                        <?php foreach ($barang as $key => $value): ?>
                                        <option value="<?= $value->id ?>" > <?= $value->id_komponen.' - '.$value->name ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Jumlah</label>
                                    <input class="form-control" type="number" required name="jumlah" id="jumlah">
                                </div>
                                <div class="form-group">
                                    <label>Tanggal <?= $jenis == '1' ? 'Masuk' : 'Keluar' ?></label>
                                    <input class="form-control" type="date" required name="tanggal_masuk" id="tanggal_masuk">
                                </div>
                                <div class="form-group">
                                    <label> Keterangan</label>
                                    <textarea class="form-control" type="text" name="note" id="note"></textarea>
                                </div>
                            </fieldset>
                            <div>
                                <input type="hidden" name="id" id="id">
                                <input type="hidden" name="id_barang" id="id_barangtx">
                                <input type="hidden" name="jumlahx" id="jumlahx">
                                <input type="hidden" name="jenis" value="<?= $jenis ?>">
                                <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <!-- Modal -->

        <!-- Footer -->
        <?php $this->load->view('layout/footer') ?>
        <!-- End Footer -->

        <script type="text/javascript">
            var url = "<?= site_url().$controller ?>";

            $('.edit').click(function(){
                $.ajax({
                    url: url + '/edit/' + $(this).data('id'),
                    type:'GET',
                    dataType: 'json',
                    success: function(data){
                        $("#id").val(data['list_edit']['id']);
                        $("#id_barang").val(data['list_edit']['id_barang']); 
                        $("#id_barangtx").val(data['list_edit']['id_barang']);                                    
                        $("#jumlah").val(data['list_edit']['jumlah']);  
                        $("#jumlahx").val(data['list_edit']['jumlah']);                                    
                        $("#note").val(data['list_edit']['note']);           

                        /*handle select 2*/
                        setoption(data['list_edit']['id_komponen']);
                        //$("#id_barang").val(data['list_edit']['id_barang']).trigger('change');
                        /*handle select 2*/                         

                        /*khusus*/
                        const d = new Date(data['list_edit']['tanggal_masuk']);
                        const dtf = new Intl.DateTimeFormat('en', { year: 'numeric', month: '2-digit', day: '2-digit' }) ;
                        const [{ value: mo },,{ value: da },,{ value: ye }] = dtf.formatToParts(d) ;
                        console.log(ye +'-'+mo +'-'+da);
                        $("#tanggal_masuk").val(ye +'-'+mo +'-'+da);   
                        /*khusus*/
                        $(".edit-modal").modal('show');
                    }                
                }); 
            })

            $('.delete').click(function () {
                var id = $(this).data('id') ;
                swal({
                    title: 'Apakah Data ini ingin di Hapus? ',
                    showCancelButton: true,
                    confirmButtonColor: '#4fa7f3',
                    cancelButtonColor: '#d57171',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    window.location = url + '/delete/' + id ;
                })
            });

            function setoption(argument) {
                console.log(argument);
                $.ajax({
                    url: url + '/getbarang',
                    type:'POST',
                    data: { search: argument }, 
                    dataType: 'json',
                }).then(function (data) {

                    // Set the value, creating a new option if necessary
                    if ($('#id_barang').find("option[value='" + data[0].id + "']").length) {
                        $('#id_barang').val(data[0].id).trigger('change');
                    } else { 
                        // Create a DOM Option and pre-select by default
                        var option = new Option(data[0].text, data[0].id, true, true);
                        // Append it to the select
                        $('#id_barang').append(option).trigger('change');
                    }
                });
            }
            
            $(function() {
                $('.select2').select2({
                  ajax: {
                    url: url + '/getbarang',
                    type:'POST',
                    data: function (params) {
                      var query = {
                        search: params.term,
                      }
                      return query;
                    },
                    processResults: function (data) {
                      return {
                        results: data
                      };
                    }
                  },
                  placeholder: 'Masukan minimum 3 karakter',
                  minimumInputLength: 3,
                });
            });
            
        </script>
    </body>
</html>