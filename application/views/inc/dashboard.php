<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('layout/header') ?>

        <style>
            h6 {
                text-transform: uppercase;
            }
            h6.inline {
                display: inline-block;
                margin-left: 20px;
            }
            .status {
                margin-top: 10px;
            }

            .filled {
                border: 4px solid #467ed1;
            }

            .unfilled {
                border: 4px solid #F44336;
            }

            .tilebox-one i.unfilledbox {
                background: linear-gradient(to top, #E91E63, #F44336);    
            }
            
        </style>
    </head>
    <body>
        <!-- Navigation Bar-->
        <?php $this->load->view('layout/navigation') ?>
        <!-- End Navigation Bar-->
        
        <!-- wrapper -->
        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                    <li class="breadcrumb-item active">Dashboard</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Welcome</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-xl-7">
                        <div class="card-box">
                            <h4 class="header-title">Transactions History</h4>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="text-center mt-3">
                                        <h6 class="font-normal text-muted font-14">Conversion Rate</h6>
                                        <h6 class="font-18"><i class="mdi mdi-arrow-up-bold-hexagon-outline text-success"></i> <span class="text-dark">1.78%</span> <small></small></h6>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="text-center mt-3">
                                        <h6 class="font-normal text-muted font-14">Average Order Value</h6>
                                        <h6 class="font-18"><i class="mdi mdi-arrow-up-bold-hexagon-outline text-success"></i> <span class="text-dark">25.87</span> <small>USD</small></h6>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="text-center mt-3">
                                        <h6 class="font-normal text-muted font-14">Total Wallet Balance</h6>
                                        <h6 class="font-18"><i class="mdi mdi-arrow-up-bold-hexagon-outline text-success"></i> <span class="text-dark">87,4517</span> <small>USD</small></h6>
                                    </div>
                                </div>
                            </div>

                            <canvas id="transactions-chart" height="350" class="mt-4"></canvas>
                        </div>
                    </div>
                    <div class="col-xl-5">
                        <div class="card-box">
                            <h4 class="header-title">Finance History 2019</h4>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="text-center mt-3">
                                        <h6 class="font-normal text-muted font-14">Conversion Rate</h6>
                                        <h6 class="font-18"><i class="mdi mdi-arrow-up-bold-hexagon-outline text-success"></i> <span class="text-dark">3.94%</span> <small></small></h6>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="text-center mt-3">
                                        <h6 class="font-normal text-muted font-14">Average</h6>
                                        <h6 class="font-18"><i class="mdi mdi-arrow-down-bold-hexagon-outline text-danger"></i> <span class="text-dark">16.25</span> <small>USD</small></h6>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="text-center mt-3">
                                        <h6 class="font-normal text-muted font-14">Total</h6>
                                        <h6 class="font-18"><i class="mdi mdi-arrow-up-bold-hexagon-outline text-success"></i> <span class="text-dark">124,858.67</span> <small>USD</small></h6>
                                    </div>
                                </div>
                            </div>

                            <canvas id="sales-history" height="350" class="mt-4"></canvas>
                        </div>
                    </div>
                </div>

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- Footer -->
        <?php $this->load->view('layout/footer') ?>
        <!-- End Footer -->

        <script src="<?= base_url().'assets/front/'?>assets/pages/jquery.dashboard.init.js"></script>
    </body>
</html>