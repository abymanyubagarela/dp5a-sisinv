<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('layout/header') ?>
    </head>
    <body>
        <!-- Navigation Bar-->
        <?php $this->load->view('layout/navigation') ?>
        <!-- End Navigation Bar-->
        
        <!-- wrapper -->
        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                    <li class="breadcrumb-item active"> <?= $title ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <!-- Main -->
                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <h5>Halaman Data <?= $title ?></h5>
                            
                            <button type="button" class="btn btn-success waves-light waves-effect w-md" data-toggle="modal" data-target=".create">Tambah</button>
                            
                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Bidang</th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $i = 1 ;
                                    foreach ($list as $key =>$row) { ?>
                                        <tr>
                                            <td><?= $i++ ?></td>
                                            <td><?= strtoupper($row->id_department) ?></td>
                                            <td><?= ucwords($row->name) ?></td>
                                            <td><?= $row->is_kasi ? 'Kasie' : 'Bendahara' ?></td>
                                            <td align="center">
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-warning btn-xs edited" data-id="<?= $row->id; ?>" > <i class="fa fa-wrench"></i> </button>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End Main -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- Modal -->
        <div class="modal fade bs-example-modal-lg create" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Penambahan Data</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/updatepengaturan'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>Department</label>
                                    <select class="form-control form-bidang" required name="id_department">
                                        <option> Pilih Bidang </option>
                                        <?php foreach ($notdepartment as $key => $d): ?>
                                            <option value="<?= $d->id ?>" > <?= strtoupper($d->name) ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Kasie 1</label>
                                    <select class="form-control user-bidang" required name="kasie1">
                                        
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Kasie 2</label>
                                    <select class="form-control user-bidang" required name="kasie2">
                                        
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Bendahara Bidang</label>
                                    <select class="form-control user-bidang" required name="bendahara">
                                        
                                    </select>
                                </div>

                            </fieldset>

                            <div>
                                <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <div class="modal fade bs-example-modal-lg edit-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Penambahan Data</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/updatepengaturanedit'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>Department</label>
                                    <select class="form-control"required id="id_department">
                                        <?php foreach ($department as $key => $d): ?>
                                            <option value="<?= $d->id ?>" > <?= strtoupper($d->name) ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>

                                 <div class="form-group">
                                    <label>Role</label>
                                    <select class="form-control" required id="jabatan">
                                        <option value="1"> Kasie </option>    
                                        <option value="2"> Bendahara </option>    
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>User</label>
                                    <select class="form-control user-bidang-edit" required name="jabatan-next" id="name">
                                    </select>
                                </div>
                            </fieldset>

                            <div>
                                <input type="hidden" name="jabatan-prev" id="jabatan-prev">
                                <input type="hidden" name="jabatan-role" id="jabatan-role">
                                <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <!-- Modal -->

        <!-- Footer -->
        <?php $this->load->view('layout/footer') ?>
        <!-- End Footer -->

        <script type="text/javascript">
            var url = "<?= site_url().$controller ?>";

            $('.edited').click(function(){

                $.ajax({
                    url: url + '/edit/' + $(this).data('id'),
                    type:'GET',
                    dataType: 'json',
                    success: function(data){
                        var id = data['list_edit']['id'];
                        $("#jabatan-prev").val(data['list_edit']['id']);
                        $("#id_department").val(data['list_edit']['id_department']);   
                        if(data['list_edit']['is_kasi'] === '1') {  
                            $("#jabatan").val('1');   
                            $("#jabatan-role").val('1');
                        } else {
                            $("#jabatan").val('2');   
                            $("#jabatan-role").val('2');
                        }

                        $('#id_department').prop('disabled', true);
                        $('#jabatan').prop('disabled', true);

                        $.ajax({
                            url: url + '/getlistbybidang/' + data['list_edit']['id_department'],
                            type:'GET',
                            dataType: 'json',
                            success: function(data){
                                $('.user-bidang-edit').children().remove().end();
                                $.each(data['list_bidang'], function( k, v ) {
                                    $(".user-bidang-edit").append('<option value="' +v.id+ '">' + v.name.toUpperCase() +'</option>');
                                });  
                                $(".user-bidang-edit").val(id);    
                            }                
                        }); 
                        
                        $(".edit-modal").modal('show');
                    }                
                }); 
            })
            
            $( ".form-bidang" ).change(function() {
                var id = $( ".form-bidang" ).val();

                $.ajax({
                    url: url + '/getlistbybidang/' + id,
                    type:'GET',
                    dataType: 'json',
                    success: function(data){
                        $('.user-bidang').children().remove().end();
                        $.each(data['list_bidang'], function( k, v ) {
                            $(".user-bidang").append('<option value="' +v.id+ '">' + v.name.toUpperCase() +'</option>');
                        });
                    }                
                }); 
            });

        </script>
    </body>
</html>