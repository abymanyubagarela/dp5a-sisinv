<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('layout/header') ?>
    </head>
    <body>
        <!-- Navigation Bar-->
        <?php $this->load->view('layout/navigation') ?>
        <!-- End Navigation Bar-->
        
        <!-- wrapper -->
        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                    <li class="breadcrumb-item active"> <?= $title ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <!-- Main -->
                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <h5>Halaman Data <?= $title ?></h5>
                            
                            <button type="button" class="btn btn-success waves-light waves-effect w-md" data-toggle="modal" data-target=".create">Tambah</button>
                            
                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>NIP</th>
                                        <th>Department</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $i = 1 ;
                                    foreach ($list as $key =>$row) { ?>
                                        <tr>
                                            <td><?= $i++ ?></td>
                                            <td><?= ucwords($row->name) ?></td>
                                            <td><?= $row->nik ?></td>
                                            <td><?= strtoupper($row->id_department) ?></td>
                                            <td><?= $row->email ?></td>
                                            <td align="center">
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-warning btn-xs edited" data-id="<?= $row->id; ?>" > <i class="fa fa-wrench"></i> </button>
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-danger btn-xs delete" data-id="<?= $row->id; ?>"> <i class="fa fa-remove"></i> </button>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End Main -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- Modal -->
        <div class="modal fade bs-example-modal-lg create" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Penambahan Data</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/insert'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input class="form-control" type="text" required name="name">
                                </div>

                                <div class="form-group">
                                    <label>NIP</label>
                                    <input class="form-control" type="text" required name="nik">
                                </div>

                                <div class="form-group">
                                    <label>Department</label>
                                    <select class="form-control"required name="id_department">
                                        <?php foreach ($department as $key => $d): ?>
                                            <option value="<?= $d->id ?>" > <?= strtoupper($d->name) ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Role</label>
                                    <select class="form-control"required name="id_role">
                                        <?php foreach ($role as $key => $r): ?>
                                            <?php if ($r->id != '1'): ?>
                                                <option value="<?= $r->id ?>" > <?= $r->name ?> </option>    
                                            <?php endif ?>                                            
                                        <?php endforeach ?>                                     
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" type="email" required name="email">
                                </div>

                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" type="password" required name="password">
                                </div>
                            </fieldset>

                            <div>
                                <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <div class="modal fade bs-example-modal-lg edit-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Penambahan Data</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/update'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input class="form-control" type="text" required name="name" id="name">
                                </div>

                                <div class="form-group">
                                    <label>NIP</label>
                                    <input class="form-control" type="text" required name="nik" id="nik">
                                </div>

                                <div class="form-group">
                                    <label>Department</label>
                                    <select class="form-control"required name="id_department" id="id_department">
                                        <?php foreach ($department as $key => $d): ?>
                                            <option value="<?= $d->id ?>" > <?= strtoupper($d->name) ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Role</label>
                                    <select class="form-control"required name="id_role" id="id_role">
                                        <?php foreach ($role as $key => $r): ?>
                                            <?php if ($r->id != '1'): ?>
                                                <option value="<?= $r->id ?>" > <?= $r->name ?> </option>    
                                            <?php endif ?>              
                                        <?php endforeach ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" type="email" required name="email" id="email">
                                </div>

                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" type="password" required name="password" id="password">
                                </div>
                            </fieldset>

                            <div>
                                <input type="hidden" name="id" id="id">
                                <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <!-- Modal -->

        <!-- Footer -->
        <?php $this->load->view('layout/footer') ?>
        <!-- End Footer -->

        <script type="text/javascript">
            var url = "<?= site_url().$controller ?>";

            $('.edited').click(function(){
                $.ajax({
                    url: url + '/edit/' + $(this).data('id'),
                    type:'GET',
                    dataType: 'json',
                    success: function(data){
                        
                        $("#id").val(data['list_edit']['id']);
                        $("#name").val(data['list_edit']['name']);                                    
                        $("#nik").val(data['list_edit']['nik']);                                    
                        $("#id_department").val(data['list_edit']['id_department']);   
                        $("#id_role").val(data['list_edit']['id_role']);   
                        $("#email").val(data['list_edit']['email']);                                    
                        $("#password").val(data['list_edit']['password']);                                    

                        $(".edit-modal").modal('show');
                    }                
                }); 
            })

            $('.delete').click(function () {
                var id = $(this).data('id') ;
                swal({
                    title: 'Apakah Data ini ingin di Hapus? ',
                    showCancelButton: true,
                    confirmButtonColor: '#4fa7f3',
                    cancelButtonColor: '#d57171',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    window.location = url + '/delete/' + id ;
                })
            });

        </script>
    </body>
</html>