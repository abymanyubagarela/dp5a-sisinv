<!DOCTYPE html>

<html>
    <head>
        <?php $this->load->view('layout/header') ?>
        <style type="text/css">
            .show {
                margin-top: 30px;
            }

            @media print {
                .print {
                    display: none;
                }

                .wrapper {
                    padding-top: 10px;
                }

                .table {
                    font-size: 12px;
                }
            }

            .table thead th {
                vertical-align: middle;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <!-- Navigation Bar-->
        <?php $this->load->view('layout/navigation') ?>
        <!-- End Navigation Bar-->
        
        <!-- wrapper -->
        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                    <li class="breadcrumb-item active"> <?= $title ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <!-- Main -->
                <div class="row print">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <h5>Form <?= $title ?></h5>
                            <br>
                            <?php echo form_open_multipart($controller.'/pengajuan'); ?>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Bulan</label>
                                            <select class="form-control" name="bulan">
                                                <?php for($i=1;$i<=12;$i++) { ?>
                                                    <option value="<?= $i ?>" <?= !empty($filter) ? ($filter['bulan'] == $i ? 'selected' : '') : '' ; ?> > <?= date('F',strtotime('2018-'.$i.'-01')) ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Tahun</label>
                                            <select class="form-control" name="tahun">
                                            <?php for($i=1;$i<=10;$i++) { ?>
                                                <option value="<?= $i+2015 ?>" <?= !empty($filter) ? ($filter['tahun'] == $i+'2015' ? 'selected' : '') : '' ; ?>> <?= $i+2015 ?> </option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <button type="submit" class="btn btn-info waves-light waves-effect w-md show">Tampilkan</button>
                                        <?php if (!empty($pengajuan)): ?>
                                            <button type="button" class="btn btn-success waves-light waves-effect w-md show preview">Cetak</button>     
                                        <?php endif ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <?php if (!empty($filter)): ?>
                        <div class="card-box table-responsive">
                            <h6 align="center">Rincian Pengajuan Panjar Bulan <?= date('F', mktime(0, 0, 0, $filter['bulan'], 10)) .' '. $filter['tahun'] ?></h6>

                            <table class="table table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="10%">Kode Keg. Rek.</th>
                                        <th>Uraian</th>
                                        <th>Jumlah</th>
                                        <th width="10%">Total (Rp)</th>
                                        <th width="10%">GU (Rp)</th>
                                        <th width="10%">TU (Rp)</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    <?php if (!empty($pengajuan)): ?>
                                        <?php 
                                            $totalall = 0 ; 
                                            $totalgu = 0 ; 
                                            $totaltu = 0 ;   
                                        ?>
                                        <?php foreach ($pengajuan as $key => $row): ?>
                                            <?php $total = 0 ; ?>
                                            <tr>
                                                <td> &nbsp;</td>
                                                <td> <b><?= strtoupper($row->name) ?></b></td>
                                                <td> &nbsp;</td>
                                                <td> &nbsp;</td>
                                                <td> &nbsp;</td>
                                                <td> &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td><b><?= str_replace(' ', '.', $row->kode) ?></b></td>
                                                <td><b><?= ucwords($row->description) ?></b></td>
                                                <td> &nbsp;</td>
                                                <td> &nbsp;</td>
                                                <td> &nbsp;</td>
                                                <td> &nbsp;</td>
                                            </tr>

                                            <?php if (!empty($row->detail)): ?>
                                                <?php foreach ($row->detail as $key => $detail): ?>
                                                    <?php 
                                                        $total = $total + $detail->nominal ; 
                                                        $totalall = $totalall + $total ;
                                                        if($row->jenis == '1') {
                                                            $totalgu = $totalgu + $detail->nominal ; 
                                                        } else {
                                                            $totaltu = $totaltu + $detail->nominal ; 
                                                        }
                                                    ?>
                                                    <tr>
                                                        <td><?= str_replace(' ', '.', $detail->koderekening) ?></td>
                                                        <td><?= ucwords($detail->descriptionrekening) ?></td>
                                                        <td> <?= number_format($detail->nominal) ?></td>
                                                        <td> &nbsp;</td>
                                                        <td> &nbsp;</td>
                                                        <td> &nbsp;</td>
                                                    </tr>
                                                <?php endforeach ?>
                                                <tr>
                                                    <td> &nbsp;</td>
                                                    <td> &nbsp;</td>
                                                    <td> &nbsp;</td>
                                                    <td> <?= number_format($total) ?></td>
                                                    <td><?= $row->jenis == '1' ? ' '.number_format($total) : '' ?></td>
                                                    <td><?= $row->jenis == '2' ? ' '.number_format($total) : '' ?></td>
                                                </tr>    
                                            <?php endif ?>
                                            <tr>
                                               <td colspan="6"> &nbsp;</td> 
                                            </tr>
                                        <?php endforeach ?>
                                            <tr>
                                                <th> &nbsp;</th>
                                                <th> &nbsp;</th>    
                                                <th width="15%"> Jumlah Rincian UP</th>
                                                <th> <?= number_format($totalall) ?></th>
                                                <th> <?= number_format($totalgu) ?></th>
                                                <th> <?= number_format($totaltu) ?></th>
                                            </tr>  
                                            <tr>
                                                <th> &nbsp;</th>
                                                <th> &nbsp;</th>    
                                                <th width="15%"> GU</th>
                                                <th> <?= number_format($totalgu) ?></th>
                                                <th> &nbsp;</th>
                                                <th> &nbsp;</th>
                                            </tr>  
                                            <tr>
                                                <th> &nbsp;</th>
                                                <th> &nbsp;</th>    
                                                <th width="15%"> TU</th>
                                                <th> <?= number_format($totaltu) ?></th>
                                                <th> Sisa Kas</th>
                                                <th> GU</th>
                                            </tr>  
                                            <tr>
                                                <th> &nbsp;</th>
                                                <th> &nbsp;</th>    
                                                <th> &nbsp;</th>
                                                <th> &nbsp;</th>
                                                <th> *</th>
                                                <th> *</th>
                                            </tr>  
                                            <tr>
                                                <th> &nbsp;</th>
                                                <th> &nbsp;</th>    
                                                <th width="15%"> Total</th>
                                                <th> <?= number_format($totalall) ?></th>
                                                <th> &nbsp;</th>   
                                                <th> &nbsp;</th>   
                                            </tr>  
                                    <?php endif ?>
                                </tbody>
                            </table>
                        </div>
                        <?php endif ?>
                    </div>
                </div>
                <!-- End Main -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- Footer -->
        <?php $this->load->view('layout/footer') ?>
        <!-- End Footer -->

        <script type="text/javascript">
            $('.preview').click(function(e) {
                window.print();
            })
        </script>
    </body>
</html>