<!DOCTYPE html>

<html>
    <head>
        <?php $this->load->view('layout/header') ?>
        <style type="text/css">
            .form-group span {
                color: red;
                display: none;
            }
            .pph21 {
                margin-bottom: 10px;
            }
        </style>
    </head>
    <body>
        <!-- Navigation Bar-->
        <?php $this->load->view('layout/navigation') ?>
        <!-- End Navigation Bar-->
        
        <!-- wrapper -->
        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                    <li class="breadcrumb-item active"> <?= $title ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <!-- Main -->
                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <h5>Halaman <?= $title ?></h5>
                            <br>

                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <tr>
                                    <td width="10%">Nama Kegiatan</td>
                                    <th width="30%"><u><?= ucwords($list_edit->name) ?></u></th>
                                    <td width="10%">Tanggal</td>
                                    <th width="30%"><u><?= date('d M Y',strtotime($list_edit->tanggal)) ?></u></th>
                                </tr>

                                <tr>
                                    <td width="10%">Kegiatan</td>
                                    <th width="30%" colspan="3"><u><?= ucfirst($list_edit->kode.' - '.$list_edit->description) ?></u></th>
                                </tr>

                                <tr>
                                    <td width="10%">Department</td>
                                    <th width="30%" colspan="3"><u><?= strtoupper($list_edit->id_department) ?></u></th>
                                </tr>

                                <tr>
                                    <td width="10%">Kode Rekening</td>
                                    <th width="30%"><u><?= ucwords($rekening->kode) ?></u></th>
                                    <td width="10%">Keterangan</td>
                                    <th width="30%"><u><?= ucfirst($rekening->description) ?></u></th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <button type="button" class="btn btn-success waves-light waves-effect w-md" data-toggle="modal" data-target=".create">Tambah</button>
                            
                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>ID Transaksi</th>
                                        <th>ID Pekerjaan</th>
                                        <th>Uraian</th>
                                        <th>Realisasi</th>
                                        <th>Tanggal Pajak</th>
                                        <th>PPH21</th>
                                        <th>PPH22</th>
                                        <th>PPH23</th>
                                        <th>PPN</th>
                                        <th>Final</th>
                                        <th>SSPD</th>
                                        <th>NTB</th>
                                        <th>NTPN</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $i = 1 ;
                                    foreach ($list as $key =>$row) { ?>
                                        <tr>
                                            <td><?= $i++ ?></td>
                                            <td><?= date('d M Y',strtotime($row->tanggal)) ?></td>
                                            <td><?= $row->id_transaksi ?></td>
                                            <td><?= $row->id_pekerjaan ?></td>
                                            <td><?= ucwords($row->pekerjaan) ?></td>
                                            <td>Rp. <?= number_format($row->nominal) ?></td>
                                            <td>
                                                <?php   
                                                    if ($row->tanggal_pajak == '0000-00-00') {
                                                        echo "*";
                                                    } else {
                                                        echo date('d/m/Y',strtotime($row->tanggal_pajak)) ;
                                                    }
                                                ?>
                                            </td>
                                            <td>
                                            <?php  
                                                if (!empty($row->pph21)) {
                                                    switch ($row->pph21) {
                                                        case 0:
                                                            echo "*";
                                                            break;
                                                        case 5:
                                                            echo $row->pph21." %";
                                                            break;
                                                        case 6:
                                                            echo $row->pph21." %";
                                                            break;
                                                        case 15:
                                                            echo $row->pph21." %";
                                                            break;
                                                        
                                                        default:
                                                            echo number_format($row->pph21);
                                                            break;
                                                    }
                                                } else {
                                                    echo "*";
                                                }
                                            ?>
                                            </td>
                                            <td><?= empty($row->pph22) ? '*' : $row->pph22.' %' ?></td>
                                            <td><?= empty($row->pph23) ? '*' : $row->pph23.' %' ?></td>
                                            <td><?= empty($row->ppn) ? '*' : $row->ppn.' %' ?></td>
                                            <td><?= empty($row->final) ? '*' : $row->final.' %' ?></td>
                                            <td><?= empty($row->sspd) ? '*' : $row->sspd.' %' ?></td>
                                            <td><b><?= $row->ntb ?></b></td>
                                            <td><b><?= strtoupper($row->ntpn) ?></b></td>
                                            <td align="center">
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-warning btn-xs edited" data-id="<?= $row->id; ?>" > <i class="fa fa-wrench"></i> </button>
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-danger btn-xs delete" data-id="<?= $row->id; ?>"> <i class="fa fa-remove"></i> </button>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End Main -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- Modal -->
        <div class="modal fade bs-example-modal-lg create" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Penambahan Data</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/insert/'.$id_sub); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>Tanggal</label>
                                    <input class="form-control" type="date" required name="tanggal">
                                </div>

                                <div class="form-group">
                                    <label>ID Pekerjaan</label>
                                    <input class="form-control" type="text" name="id_pekerjaan" maxlength="8" autocomplete="off">
                                </div>

                                <div class="form-group">
                                    <label>ID Transaksi</label>
                                    <input class="form-control transaksi" type="text" name="id_transaksi" maxlength="7" autocomplete="off">
                                    <span class="unique"> ID ini sudah ada di sistem</span>
                                </div>

                                <div class="form-group">
                                    <label>Uraian</label>
                                    <input class="form-control" type="text" required name="pekerjaan">
                                </div>

                                <div class="form-group">
                                    <label>Detail Uraian</label>
                                    <textarea class="form-control" type="text" name="keterangan"></textarea>
                                </div>

                                <div class="form-group">
                                    <label>Realisasi</label>
                                    <input class="form-control" type="number" required name="nominal">
                                </div>

                                <div class="form-group" style="padding-top:20px">
                                    <p>
                                        <u>Pajak (Opsional)</u>
                                    </p>

                                    <div class="form-group">
                                        <label>Tanggal Pajak</label>
                                        <input class="form-control" type="date" name="tanggal_pajak">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>PPH 21</label>
                                            <select class="form-control pph21" name="pph21">
                                                <option value="0"> Tidak ada pajak </option>
                                                <?php foreach ($pph21 as $key => $value): ?>
                                                <option value="<?= $key ?>" > <?= $value ?> </option>
                                                <?php endforeach ?>
                                                <option value="99"> Input Manual</option>
                                            </select>
                                            <input class="form-control pphcustom" type="number" placeholder="Manual PPH21 (Rp)" name="custominput">
                                        </div>

                                        <div class="col-md-4">
                                            <label>PPH 22</label>
                                            <select class="form-control" name="pph22">
                                                <option value="0"> Tidak ada pajak </option>
                                                <?php foreach ($pph22 as $key => $value): ?>
                                                <option value="<?= $key ?>" > <?= $value ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <label>PPH 23</label>
                                            <select class="form-control" name="pph23">
                                                <option value="0"> Tidak ada pajak </option>
                                                <?php foreach ($pph23 as $key => $value): ?>
                                                <option value="<?= $key ?>" > <?= $value ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 20px">
                                        <div class="col-md-4">
                                            <label>PPN</label>
                                            <select class="form-control" name="ppn">
                                                <option value="0"> Tidak ada pajak </option>
                                                <?php foreach ($ppn as $key => $value): ?>
                                                <option value="<?= $key ?>" > <?= $value ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Final</label>
                                            <select class="form-control" name="final">
                                                <option value="0"> Tidak ada pajak </option>
                                                <?php foreach ($final as $key => $value): ?>
                                                <option value="<?= $key ?>" > <?= $value ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <label>SSPD</label>
                                            <select class="form-control" name="sspd">
                                                <option value="0"> Tidak ada pajak </option>
                                                <?php foreach ($sspd as $key => $value): ?>
                                                <option value="<?= $key ?>" > <?= $value ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>NTB</label>
                                    <input class="form-control" type="text" onkeypress='validate(event)' maxlength="12" name="ntb">
                                </div>
                                <div class="form-group">
                                    <label>NTPN</label>
                                    <input class="form-control" type="text" name="ntpn" maxlength="16">
                                </div>
                            </fieldset>

                            <div>
                                <input type="hidden" name="id_rekening" value="<?= $id_sub ?>">
                                <input type="hidden" name="id_parent" value="<?= $id ?>">
                                <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right save">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <!-- Modal -->
        <div class="modal fade bs-example-modal-lg edit-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Penambahan Data</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/update/'.$id_sub); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>Tanggal</label>
                                    <input class="form-control" type="date" required name="tanggal" id="tanggal">
                                </div>

                                <div class="form-group">
                                    <label>ID Pekerjaan</label>
                                    <input class="form-control" type="text" name="id_pekerjaan" id="id_pekerjaan" maxlength="8" autocomplete="off">
                                </div>

                                <div class="form-group">
                                    <label>ID Transaksi</label>
                                    <input class="form-control transaksi" type="text" name="id_transaksi" id="id_transaksi" maxlength="7" autocomplete="off">
                                    <span class="unique"> ID ini sudah ada di sistem</span>
                                </div>

                                <div class="form-group">
                                    <label>Uraian</label>
                                    <input class="form-control" type="text" required name="pekerjaan" id="pekerjaan">
                                </div>

                                <div class="form-group">
                                    <label>Detail Uraian</label>
                                    <textarea class="form-control" type="text" name="keterangan" id="keterangan"></textarea>
                                </div>

                                <div class="form-group">
                                    <label>Realisasi</label>
                                    <input class="form-control" type="number" required name="nominal" id="nominal">
                                </div>

                                <div class="form-group" style="padding-top:20px">
                                    <p>
                                        <u>Pajak (Opsional)</u>
                                    </p>

                                    <div class="form-group">
                                        <label>Tanggal Pajak</label>
                                        <input class="form-control" type="date" name="tanggal_pajak" id="tanggal_pajak">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>PPH 21</label>
                                            <select class="form-control pph21" name="pph21" id="pph21">
                                                <option value="0"> Tidak ada pajak </option>
                                                <?php foreach ($pph21 as $key => $value): ?>
                                                <option value="<?= $key ?>" > <?= $value ?> </option>
                                                <?php endforeach ?>
                                                <option value="99"> Input Manual</option>
                                            </select>
                                            <input class="form-control pphcustom" type="number" placeholder="Manual PPH21 (Rp)" name="custominput" id="pphcustom">
                                        </div>

                                        <div class="col-md-4">
                                            <label>PPH 22</label>
                                            <select class="form-control" name="pph22" id="pph22">
                                                <option value="0"> Tidak ada pajak </option>
                                                <?php foreach ($pph22 as $key => $value): ?>
                                                <option value="<?= $key ?>" > <?= $value ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <label>PPH 23</label>
                                            <select class="form-control" name="pph23" id="pph23">
                                                <option value="0"> Tidak ada pajak </option>
                                                <?php foreach ($pph23 as $key => $value): ?>
                                                <option value="<?= $key ?>" > <?= $value ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 20px">
                                        <div class="col-md-4">
                                            <label>PPN</label>
                                            <select class="form-control" name="ppn" id="ppn">
                                                <option value="0"> Tidak ada pajak </option>
                                                <?php foreach ($ppn as $key => $value): ?>
                                                <option value="<?= $key ?>" > <?= $value ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Final</label>
                                            <select class="form-control" name="final" id="final">
                                                <option value="0"> Tidak ada pajak </option>
                                                <?php foreach ($final as $key => $value): ?>
                                                <option value="<?= $key ?>" > <?= $value ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <label>SSPD</label>
                                            <select class="form-control" name="sspd" id="sspd">
                                                <option value="0"> Tidak ada pajak </option>
                                                <?php foreach ($sspd as $key => $value): ?>
                                                <option value="<?= $key ?>" > <?= $value ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>NTB</label>
                                    <input class="form-control" type="text" onkeypress='validate(event)' maxlength="12" name="ntb" id="ntb"> 
                                </div>
                                <div class="form-group">
                                    <label>NTPN</label>
                                    <input class="form-control" type="text" name="ntpn" id="ntpn" maxlength="16">
                                </div>
                            </fieldset>

                            <div>
                                <input type="hidden" name="id" id="id">
                                <input type="hidden" name="id_parent" value="<?= $id ?>">
                                <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right save">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <!-- Modal -->

        <!-- Footer -->
        <?php $this->load->view('layout/footer') ?>
        <!-- End Footer -->
       
        <script type="text/javascript">
            $(".pphcustom").hide();
            var url = "<?= site_url().$controller; ?>";

            $('.edited').click(function(){
                $(".pphcustom").hide();
                $.ajax({
                    url: url + '/edit/' + $(this).data('id'),
                    type:'GET',
                    dataType: 'json',
                    success: function(data){
                        
                        $("#id").val(data['detail']['id']);
                        $("#id_rekening").val(data['detail']['id_rekening']);  
                        $("#id_transaksi").val(data['detail']['id_transaksi']);                                    
                        $("#id_pekerjaan").val(data['detail']['id_pekerjaan']);                                    
                        $("#tanggal").val(data['detail']['tanggal']);                                  
                        $("#pekerjaan").val(data['detail']['pekerjaan']);                                  
                        $("#keterangan").val(data['detail']['keterangan']);                                  
                        $("#nominal").val(data['detail']['nominal']);  
                        $("#tanggal_pajak").val(data['detail']['tanggal_pajak']);
                        $("#pph21").val(data['detail']['pph21']);      
                        $("#pph22").val(data['detail']['pph22']);      
                        $("#pph23").val(data['detail']['pph23']); 
                        $("#ppn").val(data['detail']['ppn']); 
                        $("#sspd").val(data['detail']['sspd']); 
                        $("#final").val(data['detail']['final']);    
                        $("#ntb").val(data['detail']['ntb']);
                        $("#ntpn").val(data['detail']['ntpn']);      
                        if(data['detail']['pph21'] != '5' && data['detail']['pph21'] != '6' && data['detail']['pph21'] != '15') {
                            $(".pphcustom").show();
                            $("#pph21").val(99);    
                            $("#pphcustom").val(data['detail']['pph21']);      
                        } 
                        $(".edit-modal").modal('show');
                    }                
                }); 
            })

            $('.delete').click(function () {
                var id = $(this).data('id') ;

                swal({
                    title: 'Apakah Data ini ingin di Hapus? ',
                    showCancelButton: true,
                    confirmButtonColor: '#4fa7f3',
                    cancelButtonColor: '#d57171',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    $.ajax({
                        url: url + '/delete/' + id,
                        type:'POST',
                        dataType: 'json',
                        success: function(data){
                            location.reload();    
                        }                
                    }); 
                })
            });
            
            $(".transaksi").keyup(function() {
                $.ajax({
                  url: url + '/checktransaksi/',
                  type: "post", 
                  data: { 
                    id_transaksi: $(".transaksi").val(), 
                  },
                  success: function(response) {
                    if(response.detail) {
                        $(".unique").show();
                        $(".save").hide();
                    } else {
                        $(".unique").hide();
                        $(".save").show();
                    }
                  },
                  error: function(xhr) {
                    alert('error');
                  }
                });
            });

            $(".pph21").change(function(e) {
                var _val = $(this).find(":selected").val() ;
                if(_val == '99') {
                    $(".pphcustom").show();
                } else {
                    $(".pphcustom").hide();
                }
            });

            $("#pph21").change(function(e) {
                var _val = $(this).find(":selected").val() ;
                if(_val == '99') {
                    $("#pphcustom").show();
                } else {
                    $("#pphcustom").hide();
                }
            });

            $(".pphcustom").keyup(function() {
                var _val = $(".pphcustom").val() ;
                if( _val > 0 ) {
                    $("#custominput").val(_val) ;
                } else {
                    $("#custominput").val() ;
                }
                
            });

            function validate(evt) {
              var theEvent = evt || window.event;

              // Handle paste
              if (theEvent.type === 'paste') {
                  key = event.clipboardData.getData('text/plain');
              } else {
              // Handle key press
                  var key = theEvent.keyCode || theEvent.which;
                  key = String.fromCharCode(key);
              }
              var regex = /[0-9]|\./;
              if( !regex.test(key) ) {
                theEvent.returnValue = false;
                if(theEvent.preventDefault) theEvent.preventDefault();
              }
            }
        </script>
    </body>
</html>