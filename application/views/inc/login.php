<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('layout/header') ?>

        <style type="text/css">
            .footer {
                display: none;
            }
        </style>
    </head>
    <body class="bg-accpunt-pages">
        <!-- wrapper -->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="wrapper-page">

                            <div class="account-pages">
                                <div class="account-box">
                                    <div class="account-logo-box">
                                        <h2 class="text-uppercase text-center">
                                            <!-- Text Signo -->
                                            <a href="#" class="logo">
                                                <span class="logo-large"> Sistem Inventori</span>
                                            </a>
                                        </h2>

                                        <h6 class="text-uppercase text-center font-bold mt-4">Sign In</h6>
                                    </div>
                                    <div class="account-content">
                                        
                                            <?php echo form_open_multipart($controller.'/loginpost'); ?>

                                            <div class="form-group m-b-20 row">
                                                <div class="col-12">
                                                    <label for="emailaddress">Email</label>
                                                    <input class="form-control" type="text" required placeholder="Enter your email" name="email">
                                                </div>
                                            </div>

                                            <div class="form-group row m-b-20">
                                                <div class="col-12">
                                                    <label for="password">Password</label>
                                                    <input class="form-control" type="password" required placeholder="Enter your password" name="password">
                                                </div>
                                            </div>

                                            <br>
                                            <div class="form-group row text-center m-t-10">
                                                <div class="col-12">
                                                    <button class="btn btn-block btn-gradient waves-effect waves-light" type="submit">Sign In</button>
                                                </div>
                                            </div>

                                        </form>

                                        <div class="row m-t-50">
                                            <div class="col-sm-12 text-center">
                                                <p class="text-muted">Don't have an account ? <a href="#" class="text-dark m-l-5"><b>Ask Admin</b></a></p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- end card-box-->
                        </div>
                        <!-- end wrapper -->
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer -->
        <?php $this->load->view('layout/footer') ?>
        <!-- End Footer -->
    </body>
</html>