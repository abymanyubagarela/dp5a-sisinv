<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('layout/header') ?>
    </head>
    <body>
        <!-- Navigation Bar-->
        <?php $this->load->view('layout/navigation') ?>
        <!-- End Navigation Bar-->
        
        <!-- wrapper -->
        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                    <li class="breadcrumb-item active"> <?= $title ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <!-- Main -->
                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <h5> Data <?= $title ?></h5>
                            
                            <button type="button" class="btn btn-success waves-light waves-effect w-md mb-2" data-toggle="modal" data-target=".create">Tambah</button> 

                            <table id="datatable-buttons-barang" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Komponen</th>
                                        <th>Nama</th>
                                        <th>Jenis Barang</th>
                                        <th>Jumlah</th>
                                        <th>Satuan</th>
                                        <th>Harga (Rp.) </th>
                                        <th>Pajak (%)</th>
                                        <th>Rekening</th>
                                        <th>Spesifikasi</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- <?php 
                                    $i = 1 ;
                                    foreach ($list as $key =>$row) { ?>
                                        <tr>
                                            <td><?= $i++ ?></td>
                                            <td><?= $row->id_komponen ?></td>
                                            <td><?= $row->name ?></td>  
                                            <td><?= $row->id_jenis ?></td>
                                            <td><?= $row->id_satuan ?></td>
                                            <td><?= $row->harga ?></td>
                                            <td><?= $row->pajak ?> %</td>
                                            <td><?= $row->id_rekening ?></td>
                                            <td><?= $row->note ?></td>
                                            <td align="center">
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-warning btn-xs edit" data-id="<?= $row->id; ?>" > <i class="fa fa-wrench"></i> </button>
                                                <?php if (true) { ?>
                                                    <button type="button" class="btn btn-icon waves-effect waves-light btn-danger btn-xs delete" data-id="<?= $row->id; ?>"> <i class="fa fa-remove"></i> </button>
                                                <?php } ?>                                                
                                            </td>
                                        </tr>
                                    <?php } ?> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End Main -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- Modal -->
        <div class="modal fade bs-example-modal-lg create" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Penambahan Data</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/insert'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>Kode Komponen</label>
                                    <input class="form-control" type="text" required name="id_komponen">
                                </div>
                                <div class="form-group">
                                    <label>Nama Barang</label>
                                    <input class="form-control" type="text" required name="name">
                                </div>
                                <div class="form-group">
                                    <label>Jenis Barang</label>
                                    <select class="form-control" name="id_jenis" required>
                                        <?php foreach ($jenis as $key => $value): ?>
                                        <option value="<?= $value->id ?>" > <?= $value->name ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Satuan</label>
                                    <select class="form-control select2" name="id_satuan" required>
                                        <?php foreach ($satuan as $key => $value): ?>
                                        <option value="<?= $value->id ?>" > <?= $value->name ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Rekening</label>
                                    <select class="form-control select2" name="id_rekening" required>
                                        <?php foreach ($rekening as $key => $value): ?>
                                        <option value="<?= $value->id ?>" > <?= $value->kode.' - '.$value->description ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Harga Satuan</label>
                                    <input class="form-control" type="number" required name="harga">
                                </div>
                                <div class="form-group">
                                    <label>Pajak</label>
                                    <input class="form-control" type="number" name="pajak">
                                </div>

                                <div class="form-group">
                                    <label> Spesifikasi</label>
                                    <textarea class="form-control" type="text" name="note"></textarea>
                                </div>
                            </fieldset>

                            <div>
                                <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <div class="modal fade bs-example-modal-lg edit-modal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Ubah Data</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/update'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>Kode Komponen</label>
                                    <input class="form-control" type="text" required name="id_komponen" id="id_komponen">
                                </div>
                                <div class="form-group">
                                    <label>Nama Barang</label>
                                    <input class="form-control" type="text" required name="name" id="name">
                                </div>
                                <div class="form-group">
                                    <label>Jenis Barang</label>
                                    <select class="form-control" name="id_jenis" required id="id_jenis">
                                        <?php foreach ($jenis as $key => $value): ?>
                                        <option value="<?= $value->id ?>" > <?= $value->name ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Satuan</label>
                                    <select class="form-control select2" name="id_satuan" required id="id_satuan">
                                        <?php foreach ($satuan as $key => $value): ?>
                                        <option value="<?= $value->id ?>" > <?= $value->name ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Rekening</label>
                                    <select class="form-control select2" name="id_rekening" required id="id_rekening">
                                        <?php foreach ($rekening as $key => $value): ?>
                                        <option value="<?= $value->id ?>" > <?= $value->kode.' - '.$value->description ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Harga Satuan</label>
                                    <input class="form-control" type="number" required name="harga" id="harga">
                                </div>
                                <div class="form-group">
                                    <label>Pajak</label>
                                    <input class="form-control" type="number" name="pajak" id="pajak">
                                </div>

                                <div class="form-group">
                                    <label> Spesifikasi</label>
                                    <textarea class="form-control" type="text" name="note" id="note"></textarea>
                                </div>
                            </fieldset>
                            <div>
                                <input type="hidden" name="id" id="id">
                                <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <!-- Modal -->

        <!-- Footer -->
        <?php $this->load->view('layout/footer') ?>
        <!-- End Footer -->

        <script type="text/javascript">
            var url = "<?= site_url().$controller ?>";
            function edit(id){                
                $.ajax({
                    url: url + '/edit/' + id,
                    type:'GET',
                    dataType: 'json',
                    success: function(data){
                        $("#id").val(data['list_edit']['id']);
                        $("#id_komponen").val(data['list_edit']['id_komponen']);
                        $("#name").val(data['list_edit']['name']);   
                        $("#id_jenis").val(data['list_edit']['id_jenis']);   
                        $("#id_satuan").val(data['list_edit']['id_satuan']);
                        $("#id_rekening").val(data['list_edit']['id_rekening']);
                        $("#harga").val(data['list_edit']['harga']);   
                        $("#pajak").val(data['list_edit']['pajak']);   
                        $("#note").val(data['list_edit']['note']);   

                        /*handle select 2*/
                        $("#id_satuan").val(data['list_edit']['id_satuan']).trigger('change');
                        $("#id_rekening").val(data['list_edit']['id_rekening']).trigger('change');
                        /*handle select 2*/
                        
                        $(".edit-modal").modal('show');
                    }                
                }); 
            };

            function delet(id){
                swal({
                    title: 'Apakah Data ini ingin di Hapus? ',
                    showCancelButton: true,
                    confirmButtonColor: '#4fa7f3',
                    cancelButtonColor: '#d57171',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    window.location = url + '/delete/' + id ;
                })
            };

        </script>

        <script type="text/javascript">
            var table;
            $(document).ready(function() {
                
                $('.select2').select2();
         
                table = $('#datatable-buttons-barang').DataTable({ 
         
                    "processing": true, 
                    "serverSide": true, 
                    "order": [], 
                     
                    "ajax": {
                        "url": url + '/get_data',
                        "type": "POST"
                    },
         
                     
                    "columnDefs": [
                        { 
                            "targets": [ 0 ], 
                            "orderable": false, 
                        },
                    ],         
                });
         
            });
            
        </script>
    </body>
</html>