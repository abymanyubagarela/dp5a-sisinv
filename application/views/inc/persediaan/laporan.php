<!DOCTYPE html>

<html>
    <head>
        <?php $this->load->view('layout/header') ?>

        <style type="text/css">
            @media print {
                .print {
                    visibility: hidden;
                }

                .wrapper {
                    padding-top: 10px;
                }

                .table {
                    font-size: 8px!important;
                }

                .id {
                    max-width: 65px;
                }

                .table > thead > tr > th {
                    padding: 12px 5px!important;
                }

                #important_thing { 
                  page-break-after:always ;
                }

                @page {
                    size: landscape; 
                    margin: none;  
                }
            }

            .table thead th {
                vertical-align: middle;
                text-align: center;
            }

            .center {
                text-align: center;
            }

            .left {
                width: 50%;
                margin: 0px auto;
                /*float: left;*/
                text-align: center;
            }
            .right {
                width: 50%;
                margin: 0px auto;
                /*float: right;*/
                text-align: center;
            }
            .tilebox-one  {
                /*background: #f1f1f1;*/
                background: white;
            }
            .text-muted {
                /*border-top: 2px solid;*/
            }
            .wrapper-canvas {
                padding: none;
            }
            .signature-pad {
                border:1px dashed;
            }

            /*.table {
                font-size: 10px;
            }*/

        </style>
    </head>
    <body>
    <!-- Navigation Bar-->
    <?php $this->load->view('layout/navigation') ?>
        <!-- End Navigation Bar-->

        <!-- wrapper -->
        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                    <li class="breadcrumb-item active">
                                        <?= $title ?>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <!-- Main -->
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <h5 class="center"><?= $title ?></h5>
                            <h6 class="center">Dinas Pengendalian Penduduk, Pemberdayaan Perempuan dan Perlindungan Anak</h6>
                            <br>
                            <?php if (!empty($list_edit)): ?>
                                <table class="table no-border" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="5%">Tanggal</td>
                                        <th width="30%">
                                            <?= date('d M Y',strtotime($list_edit->tanggal_kegiatan)) ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="5%">SKPD</td>
                                        <th width="30%">
                                            <?= ucwords($list_edit->id_skpd) ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="5%">Kegiatan</td>
                                        <th width="30%">
                                            <?= ucwords($list_edit->id_kegiatan_deskripsi) ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="5%">Nilai Akhir Pemakaian</td>
                                        <th width="30%">Rp.
                                            <?= number_format($list_edit->saldo_akhir) ?>
                                        </th>
                                    </tr>
                                </table>
                                <?php else: ?>
                                    <p>Data tidak ditemukan</p>
                                <?php endif ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <table class="table table-bordered" cellspacing="0" width="100%" id="table">
                                <?php 
                                    $total_saldo_awal = 0; 
                                    $total_saldo_pembelian = 0; 
                                    $total_saldo_pemakaian = 0; 
                                    $total_saldo_akhir = 0; 
                                ?>
                                <thead>
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2" width="30%">Komponen Persediaan</th>
                                        <th colspan="2">Saldo Awal</th>
                                        <th colspan="2">Pembelian / Hibah</th>
                                        <th colspan="2">Pemakaian / Pengeluaran</th>
                                        <th colspan="2">Saldo Akhir</th>
                                        <th rowspan="2">Sisa Stok</th>
                                    </tr>
                                    <tr>
                                        <?php for($i=0;$i<4;$i++): ?>
                                            <th>T.Qty</th>
                                            <th>T.Harga ( + PPN )</th>
                                        <?php endfor ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- <tr> <td colspan="9"> &nbsp;</td> </tr> -->
                                    <?php foreach ($list as $key => $row): ?>
                                        <tr>
                                            <th colspan="11"> 
                                                <?= $row['judul'] ?>
                                            </th>
                                        </tr>
                                        <?php $i = 1; ?>    
                                        <?php foreach ($row['data'] as $k => $d): ?>
                                            <tr>
                                                <td>
                                                    <?= $i++ ?>
                                                </td>
                                                <td>
                                                    <?= $d->id_komponen.' - '.$d->name ?>
                                                </td>

                                                <!-- BERDASAR JENIS -->
                                                <?php if ($d->id_jenis == '1'): ?>
                                                <td align="center">
                                                    <?= number_format($d->jumlah_barang + $d->total) ?>
                                                </td>
                                                <td align="center">
                                                    <?php $total_saldo_awal += ($d->harga + ($d->pajak/100) * $d->harga) * ($d->jumlah_barang + $d->total) ?>
                                                    <?= number_format(($d->harga + ($d->pajak/100) * $d->harga) * ($d->jumlah_barang + $d->total)) ?>
                                                </td>
                                                <td align="center">
                                                    *
                                                </td>
                                                <td align="center">
                                                    *
                                                </td>
                                                <?php else: ?>
                                                <td align="center">
                                                    *
                                                </td>
                                                <td align="center">
                                                    *
                                                </td>
                                                <td align="center">
                                                    <?= number_format($d->jumlah_barang + $d->total) ?>
                                                </td>
                                                <td align="center">
                                                    <?php $total_saldo_pembelian += ($d->harga + ($d->pajak/100) * $d->harga) * ($d->jumlah_barang + $d->total) ?>
                                                    <?= number_format(($d->harga + ($d->pajak/100) * $d->harga) * ($d->jumlah_barang + $d->total)) ?>
                                                </td>
                                                <?php endif ?>
                                                <!-- BERDASAR JENIS -->

                                                <td align="center">
                                                    <?= number_format($d->total) ?>
                                                </td>
                                                <td align="center">
                                                    <?php $total_saldo_pemakaian += (($d->harga + ($d->pajak/100) * $d->harga) * $d->total) ?>
                                                    <?= number_format(($d->harga + ($d->pajak/100) * $d->harga) * $d->total) ?>
                                                </td>
                                                <td align="center">
                                                    <?= number_format($d->jumlah_barang + $d->total) - $d->total  ?>
                                                </td>
                                                <td align="center">
                                                    <?php $total_saldo_akhir += ($d->harga + ($d->pajak/100) * $d->harga) * (($d->jumlah_barang + $d->total) - $d->total) ?>
                                                    <?= number_format(($d->harga + ($d->pajak/100) * $d->harga) * (($d->jumlah_barang + $d->total) - $d->total)) ?>
                                                </td>
                                                <td align="center">
                                                    <?= number_format($d->jumlah_barang + $d->total) - $d->total  ?>
                                                </td>
                                            </tr>                                            
                                        <?php endforeach ?>
                                    <?php endforeach ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="2" class="center"> Total Persediaan </th>
                                        <th> &nbsp; </th>
                                        <th class="center"> <?= $total_saldo_awal != 0 ? number_format($total_saldo_awal) : '*' ?></th>
                                        <th> &nbsp; </th>
                                        <th class="center"> <?= $total_saldo_pembelian != 0 ? number_format($total_saldo_pembelian) : '*' ?></th>
                                        <th> &nbsp; </th>
                                        <th class="center"> <?= $total_saldo_pemakaian != 0 ? number_format($total_saldo_pemakaian) : '*' ?></th>
                                        <th> &nbsp; </th>
                                        <th class="center"> <?= $total_saldo_akhir != 0 ? number_format($total_saldo_akhir) : '*' ?></th>
                                        <th> &nbsp; </th>   
                                    </tr>
                                </tfoot>
                            </table>          
                            
                            <div class="pt-4 center" style="max-width: 950px;margin: 0 auto;">
                                <div class="row"  id="important_thing">
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="tilebox-one">
                                            <div class="wrapper-canvas">
                                                <img src="<?= $list_edit->kasie_ttd ?>">
                                            </div>
                                            <p class="text-muted text-uppercase mb-2 mt-2 pt-2"> 
                                                <?= ucwords($list_edit->kasi) ?>
                                            </p>
                                            <p class="text-uppercase mb-3 mt-0">Kepala Sie</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="tilebox-one">
                                            <div class="wrapper-canvas">
                                                <img src="<?= $list_edit->bendahara_ttd ?>">
                                            </div>
                                            <p class="text-muted text-uppercase mb-2 mt-2 pt-2"> 
                                                <?= ucwords($list_edit->bendahara) ?>
                                            </p>
                                            <p class="text-uppercase mb-3 mt-0">Bendahara Bidang</p>
                                        </div>
                                    </div>
                                </div> 
                            </div>                 
                        </div>
                    </div>
                </div>
                <!-- End Main -->

            </div>
            <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- Footer -->
        <?php $this->load->view('layout/footer') ?>
        <!-- End Footer -->
    </body>
</html> 