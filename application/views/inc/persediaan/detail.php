<!DOCTYPE html>

<html>

<head>
    <?php $this->load->view('layout/header') ?>

    <style type="text/css">
        .left {
            width: 25%;
            margin: 0px auto;
            /*float: left;*/
            text-align: center;
        }
        .right {
            width: 25%;
            margin: 0px auto;
            /*float: right;*/
            text-align: center;
        }
        .tilebox-one  {
            /*background: #f1f1f1;*/
            background: white;
        }
        .text-muted {
            border-top: 2px solid;
        }
        .wrapper-canvas {
            padding: none;
        }
        .signature-pad {
            border:1px dashed;
        }
    </style>
</head>

<body>
    <!-- Navigation Bar-->
    <?php $this->load->view('layout/navigation') ?>
        <!-- End Navigation Bar-->

        <!-- wrapper -->
        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                    <li class="breadcrumb-item active">
                                        <?= $title ?>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <!-- Main -->
                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <h5><?= $title ?></h5>
                            <br>
                            <?php if (!empty($list_edit)): ?>
                                <table class="table" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="5%">Tanggal</td>
                                        <th width="30%">
                                            <?= date('d M Y',strtotime($list_edit->tanggal_kegiatan)) ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="5%">SKPD</td>
                                        <th width="30%">
                                            <?= ucwords($list_edit->id_skpd) ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="5%">Kegiatan</td>
                                        <th width="30%">
                                            <?= ucwords($list_edit->id_kegiatan_deskripsi) ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="5%">Nilai Akhir</td>
                                        <th width="30%">Rp.
                                            <?= number_format($list_edit->saldo_akhir) ?>
                                        </th>
                                    </tr>
                                </table>
                                <?php else: ?>
                                    <p>Data tidak ditemukan</p>
                                <?php endif ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <button type="button" class="btn btn-success waves-light waves-effect w-md" data-toggle="modal" data-target=".create">Tambah</button>

                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Barang</th>
                                        <th>Jumlah</th>
                                        <th>Harga</th>
                                        <th>Total Harga (+PPN) </th>
                                        <th>Keterangan</th>
                                        <th>Tanggal Keluar</th>
                                        <th>Tanggal Input</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $i = 1 ;
                                    foreach ($list as $key =>$row) { ?>
                                        <tr>
                                            <td>
                                                <?= $i++ ?>
                                            </td>
                                            <td>
                                                <?= $row->id_komponen.' - '.$row->name ?>
                                            </td>
                                            <td>
                                                <?= $row->total ?>
                                            </td>
                                            <td>
                                                <?= $row->harga ?>
                                            </td>
                                            <td> 
                                                <?= number_format($row->harga * $row->jumlah + ((($row->pajak/100) *($row->harga * $row->jumlah)))) ?>
                                            </td>
                                            <td>
                                                <?= $row->note ?>
                                            </td>
                                            <td>
                                                <?= date("d M Y", strtotime($row->tanggal_masuk)); ?>
                                            </td>
                                            <td>
                                                <?= date("d M Y", strtotime($row->date_created)); ?>
                                            </td>
                                            <td align="center">
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-warning btn-xs edit" data-id="<?= $row->id; ?>"> <i class="fa fa-wrench"></i> </button>
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-danger btn-xs delete" data-id="<?= $row->id; ?>"> <i class="fa fa-remove"></i> </button>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="4"> Total </th>
                                        <th> <?= number_format($total) ?> </th>
                                        <th colspan="4  "> &nbsp; </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End Main -->

                <?php if (($this->session->userdata['auth']->is_kasi || $this->session->userdata['auth']->is_bendahara) && (count($list) > 0 )): ?>
                    
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <div class="row">
                                <?php if ($this->session->userdata['auth']->is_kasi): ?>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="tilebox-one left">
                                        <h5 class="text-uppercase mb-3 mt-0">Kepala Sie</h5>
                                        <?php if ($list_edit->kasie_ttd != null): ?>
                                            <div class="wrapper-canvas">
                                                <img src="<?= $list_edit->kasie_ttd ?>" id="signature-pad-1">
                                            </div>
                                            <h6 class="text-muted text-uppercase mb-2 mt-2 pt-2"> 
                                                <?= ucwords($this->session->userdata['auth']->nik .' - '. $this->session->userdata['auth']->name) ?>
                                            </h6>
                                        <?php else: ?>
                                            <div class="wrapper-canvas">
                                              <canvas id="signature-pad-1" class="signature-pad"></canvas>
                                            </div>
                                            <h6 class="text-muted text-uppercase mb-2 mt-2 pt-2"> 
                                                <?= ucwords($this->session->userdata['auth']->nik .' - '. $this->session->userdata['auth']->name) ?>
                                            </h6>
                                            <button type="button" class="btn btn-success waves-light waves-effect w-xs done" data-type="1" data-id="<?= $list_edit->id ?>"> Done </button>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <?php else : ?>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="tilebox-one right">
                                        <h5 class="text-uppercase mb-3 mt-0">Bendahara Bidang</h5>
                                        <?php if ($list_edit->bendahara_ttd != null): ?>
                                            <div class="wrapper-canvas">
                                                <img src="<?= $list_edit->bendahara_ttd ?>" id="signature-pad-2">
                                            </div>
                                            <h6 class="text-muted text-uppercase mb-2 mt-2 pt-2"> 
                                                <?= ucwords($this->session->userdata['auth']->nik .' - '. $this->session->userdata['auth']->name) ?>
                                            </h6>
                                        <?php else: ?>
                                            <div class="wrapper-canvas">
                                              <canvas id="signature-pad-2" class="signature-pad"></canvas>
                                            </div>
                                            <h6 class="text-muted text-uppercase mb-2 mt-2 pt-2"> 
                                                <?= ucwords($this->session->userdata['auth']->nik .' - '. $this->session->userdata['auth']->name) ?>
                                            </h6>
                                            <button type="button" class="btn btn-success waves-light waves-effect w-xs done" data-type="2" data-id="<?= $list_edit->id ?>"> Done </button>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif ?>
            </div>
            <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- Modal -->
        <div class="modal fade bs-example-modal-lg create" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Penambahan Data</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/insertdetail'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>Nama Barang</label>
                                    <select class="form-control select2 id_barang" name="id_barang" required>
                                        <?php foreach ($barang as $key => $value): ?>
                                            <option value="<?= $value->id ?>">
                                                <?= $value->id_komponen.' - '.$value->name ?>
                                            </option>
                                            <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Jumlah</label>
                                    <input class="form-control jumlah" type="number" required name="jumlah">
                                    <p class="text-danger"> * jumlah melebihi stok tersedia</p>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Keluar</label>
                                    <input class="form-control" type="date" required name="tanggal_masuk">
                                </div>
                                <div class="form-group">
                                    <label> Keterangan</label>
                                    <textarea class="form-control" type="text" name="note"></textarea>
                                </div>
                            </fieldset>

                            <div>
                                <input type="hidden" name="jenis" value="2">
                                <input type="hidden" name="id_kegiatan" value="<?= $list_edit->id ?>">
                                <button type="submit" class="submit btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                            </div>
                            </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade bs-example-modal-lg edit-modal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Penambahan Data</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/updatedetail'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>Nama Barang</label>
                                    <select class="form-control select2" id="id_barang" required disabled>
                                        <?php foreach ($barang as $key => $value): ?>
                                            <option value="<?= $value->id ?>">
                                                <?= $value->id_komponen.' - '.$value->name ?>
                                            </option>
                                            <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Jumlah</label>
                                    <input class="form-control jumlah" type="number" required name="jumlah" id="jumlah">
                                    <p class="text-danger"> * jumlah melebihi stok tersedia</p>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Keluar</label>
                                    <input class="form-control" type="date" required name="tanggal_masuk" id="tanggal_masuk">
                                </div>
                                <div class="form-group">
                                    <label> Keterangan</label>
                                    <textarea class="form-control" type="text" name="note" id="note"></textarea>
                                </div>
                            </fieldset>
                            <div>
                                <input type="hidden" name="id" id="id">
                                <input type="hidden" name="id_barang" id="id_barangtx">
                                <input type="hidden" name="jumlahx" id="jumlahx">
                                <input type="hidden" name="jenis" value="2">
                                <input type="hidden" name="id_kegiatan" id="id_kegiatan">
                                <button type="submit" class=" submit btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                            </div>
                            </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- Modal -->
        <!-- Modal -->

        <!-- Footer -->
        <?php $this->load->view('layout/footer') ?>
        <!-- End Footer -->

        <script type="text/javascript">

            var url = "<?= site_url().$controller ?>";

            $('.edit').click(function() {
                $.ajax({
                    url: url + '/editdetail/' + $(this).data('id'),
                    type: 'GET',
                    dataType: 'json',
                    success: function(data) {
                        $("#id").val(data['list_edit']['id']);
                        $("#id_barang").val(data['list_edit']['id_barang']);
                        $("#id_barangtx").val(data['list_edit']['id_barang']);
                        $("#jumlah").val(data['list_edit']['jumlah']);
                        $("#jumlahx").val(data['list_edit']['jumlah']);
                        $("#note").val(data['list_edit']['note']);
                        $("#id_kegiatan").val(data['list_edit']['id_kegiatan']);

                        /*handle select 2*/
                        setoption(data['list_edit']['id_komponen']);
                        /*handle select 2*/

                        /*khusus*/
                        const d = new Date(data['list_edit']['tanggal_masuk']);
                        const dtf = new Intl.DateTimeFormat('en', {
                            year: 'numeric',
                            month: '2-digit',
                            day: '2-digit'
                        });
                        const [{
                            value: mo
                        }, , {
                            value: da
                        }, , {
                            value: ye
                        }] = dtf.formatToParts(d);
                        console.log(ye + '-' + mo + '-' + da);
                        $("#tanggal_masuk").val(ye + '-' + mo + '-' + da);
                        /*khusus*/
                        $(".edit-modal").modal('show');
                    }
                });
            })

            $('.delete').click(function() {
                var id = $(this).data('id');
                swal({
                    title: 'Apakah Data ini ingin di Hapus? ',
                    showCancelButton: true,
                    confirmButtonColor: '#4fa7f3',
                    cancelButtonColor: '#d57171',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function() {
                    window.location = url + '/deletedetail/' + id;
                })
            });

            function setoption(argument) {
                console.log(argument);
                $.ajax({
                    url: url + '/getbarang',
                    type: 'POST',
                    data: {
                        search: argument
                    },
                    dataType: 'json',
                }).then(function(data) {

                    // Set the value, creating a new option if necessary
                    if ($('#id_barang').find("option[value='" + data[0].id + "']").length) {
                        $('#id_barang').val(data[0].id).trigger('change');
                    } else {
                        // Create a DOM Option and pre-select by default
                        var option = new Option(data[0].text, data[0].id, true, true);
                        // Append it to the select
                        $('#id_barang').append(option).trigger('change');
                    }
                });
            }

            $(function() {
                const notif =  $( ".text-danger" );
                notif.hide();
                
                $('.select2').select2({
                    ajax: {
                        url: url + '/getbarang',
                        type: 'POST',
                        data: function(params) {
                            var query = {
                                search: params.term,
                            }
                            return query;
                        },
                        processResults: function(data) {
                            return {
                                results: data
                            };
                        }
                    },
                    placeholder: 'Masukan minimum 3 karakter',
                    minimumInputLength: 3,
                });
                
                $( ".id_barang" ).change(function() {
                    notif.hide();
                    $( ".jumlah" ).val(null);
                })

                $( ".jumlah" ).keyup(function() {
                    const isCreate = $('.create').is(':visible');
                    const isEdit = $('.edit-modal').is(':visible');
                    
                    let jumlah, id_barang = null;
                    if (isCreate) {
                        jumlah = $( ".jumlah" ).val();
                        id_barang = $( ".id_barang" ).val();
                    } else {
                        jumlah = $( "#jumlah" ).val();
                        id_barang = $( "#id_barangtx" ).val();
                    }
                    
                    if(id_barang) {
                        $.ajax({
                            url: url + '/getlastsaldo',
                            type: 'POST',
                            data: { id_barang },
                            dataType: 'json',
                            success: function(data) {
                                notif.text('* jumlah melebihi stok tersedia : ' + data.jumlah);
                                if(parseInt(jumlah) > parseInt(data.jumlah)) {
                                    $( ".submit" ).hide();
                                    notif.show();
                                } else {
                                    $( ".submit" ).show();
                                    notif.hide();
                                }
                            }
                        });
                    }                   
                })
            });
        </script>
        <!-- Kanvas -->
        <?php if ($this->session->userdata['auth']->is_kasi || $this->session->userdata['auth']->is_bendahara): ?>
            <?php if ($this->session->userdata['auth']->is_kasi): ?>
                <script type="text/javascript">
                    var canvas = document.getElementById('signature-pad-1');
                </script>
            <?php else: ?>
                 <script type="text/javascript">
                    var canvas = document.getElementById('signature-pad-2');
                </script>
            <?php endif ?>       
        <?php endif ?>

        <script type="text/javascript">
            var signaturePad = new SignaturePad(canvas, {
              backgroundColor: 'rgb(255, 255, 255)' 
            });
        </script>

        <script type="text/javascript">
            

            $('.done').click(function() {
                swal({
                    title: 'Simpan tanda tangan ? ',
                    showCancelButton: true,
                    confirmButtonColor: '#4fa7f3',
                    cancelButtonColor: '#d57171',
                    confirmButtonText: 'Yes, save it!'
                }).then( res => {
                    console.log(res);
                    if(res) {
                        $.ajax({
                            url: url + '/sendsignature',
                            type: 'POST',
                            data: { sign : canvas.toDataURL('image/png'), type : $(this).data('type'), id: $(this).data('id')},
                            dataType: 'json',
                            success: function(data) {
                                swal({
                                    title: 'Berhasil',
                                    text : 'Berhasil menyimpan signature.',
                                    type : 'success',
                                    confirmButtonColor: '#4fa7f3'
                                });
                                location.reload();
                            }
                        });
                    } 
                }).catch( res => {
                    signaturePad.clear(); 
                });
            })
        </script>
        <!-- Kanvas -->
    </body>

</html>