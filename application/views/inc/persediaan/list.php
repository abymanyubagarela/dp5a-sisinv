<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('layout/header') ?>
    </head>
    <body>
        <!-- Navigation Bar-->
        <?php $this->load->view('layout/navigation') ?>
        <!-- End Navigation Bar-->
        
        <!-- wrapper -->
        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Abstack</a></li>
                                    <li class="breadcrumb-item active"> <?= $title ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <!-- Main -->
                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <h5><?= $title ?></h5>
                            <button type="button" class="btn btn-success waves-light waves-effect w-md" data-toggle="modal" data-target=".create">Tambah</button>                            
                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <!-- <th>SPKD</th> -->
                                        <th>Kegiatan</th>
                                        <th>Nilai Akhir</th>
                                        <th>Tanggal Kegiatan</th>
                                        <th>Keterangan</th>
                                        <th width="15%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $i = 1 ;
                                    foreach ($list as $key =>$row) { ?>
                                        <tr>
                                            <td><?= $i++ ?></td>
                                            <!-- <td><?= $row->id_skpd ?></td> -->
                                            <td><?= $row->id_kegiatan ?></td>
                                            <td><?= number_format($row->saldo_akhir) ?></td>
                                            <td><?= date("d M Y", strtotime($row->tanggal_kegiatan)); ?></td>
                                            <td><?= $row->keterangan ?></td>
                                            <td align="center">
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-warning btn-xs edit" data-id="<?= $row->id; ?>" > <i class="fa fa-wrench"></i> </button>
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-danger btn-xs delete" data-id="<?= $row->id; ?>"> <i class="fa fa-remove"></i> </button>
                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-info btn-xs detail" data-id="<?= $row->id; ?>"> <i class="fa fa-clipboard"></i> </button>

                                                <?php if ($row->id_kasie && $row->id_bendahara): ?>
                                                    <button type="button" class="btn btn-icon waves-effect waves-light btn-purple btn-xs laporan" data-id="<?= $row->id; ?>"> <i class="fa fa-print"></i> </button>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End Main -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- Modal -->
        <div class="modal fade bs-example-modal-lg create" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Penambahan Data</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/insert'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>SKPD</label>
                                    <select class="form-control select2" name="id_skpd" required>
                                        <?php foreach ($skpd as $key => $value): ?>
                                        <option value="<?= $value->id ?>" > <?= $value->kode.' - '.$value->name ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Kegiatan</label>
                                    <select class="form-control select2" name="id_kegiatan" required>
                                        <?php foreach ($kegiatan as $key => $value): ?>
                                        <option value="<?= $value->id ?>" > <?= $value->kode.' - '.$value->description ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal</label>
                                    <input class="form-control" type="date" required name=" tanggal_kegiatan">
                                </div>
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <textarea class="form-control" type="text" name="keterangan"></textarea>
                                </div>
                            </fieldset>

                            <div>
                                <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <div class="modal fade bs-example-modal-lg edit-modal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mySmallModalLabel">Penambahan Data</h5>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart($controller.'/update'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label>SKPD</label>
                                    <select class="form-control select2" name="id_skpd" required id="id_skpd">
                                        <?php foreach ($skpd as $key => $value): ?>
                                        <option value="<?= $value->id ?>" > <?= $value->kode.' - '.$value->name ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Kegiatan</label>
                                    <select class="form-control select2" name="id_kegiatan" required id="id_kegiatan">
                                        <?php foreach ($kegiatan as $key => $value): ?>
                                        <option value="<?= $value->id ?>" > <?= $value->kode.' - '.$value->description ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal</label>
                                    <input class="form-control" type="date" required name="tanggal_kegiatan" id="tanggal_kegiatan">
                                </div>
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <textarea class="form-control" type="text" name="keterangan" id="keterangan"></textarea>
                                </div>
                            </fieldset>
                            <div>
                                <input type="hidden" name="id" id="id">\
                                <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <!-- Modal -->

        <!-- Footer -->
        <?php $this->load->view('layout/footer') ?>
        <!-- End Footer -->

        <script type="text/javascript">
            var url = "<?= site_url().$controller ?>";

            $('.edit').click(function(){
                $.ajax({
                    url: url + '/edit/' + $(this).data('id'),
                    type:'GET',
                    dataType: 'json',
                    success: function(data){
                        $("#id").val(data['list_edit']['id']);
                        $("#tanggal_kegiatan").val(data['list_edit']['tanggal_kegiatan']);  
                        $("#keterangan").val(data['list_edit']['keterangan']);                                    


                        /*handle select 2*/
                        $("#id_skpd").val(data['list_edit']['id_skpd']).trigger('change');
                        $("#id_kegiatan").val(data['list_edit']['id_kegiatan']).trigger('change');
                        /*handle select 2*/

                        /*khusus*/
                        const d = new Date(data['list_edit']['tanggal_kegiatan']);
                        const dtf = new Intl.DateTimeFormat('en', { year: 'numeric', month: '2-digit', day: '2-digit' }) ;
                        const [{ value: mo },,{ value: da },,{ value: ye }] = dtf.formatToParts(d) ;
                        console.log(ye +'-'+mo +'-'+da);
                        $("#tanggal_kegiatan").val(ye +'-'+mo +'-'+da);   
                        /*khusus*/
                        $(".edit-modal").modal('show');
                    }                
                }); 
            })

            $('.delete').click(function () {
                var id = $(this).data('id') ;
                swal({
                    title: 'Apakah Data ini ingin di Hapus? ',
                    showCancelButton: true,
                    confirmButtonColor: '#4fa7f3',
                    cancelButtonColor: '#d57171',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    window.location = url + '/delete/' + id ;
                })
            });

            $('.detail').click(function(e){
                e.preventDefault();
                window.location = url + '/detail/' + $(this).data('id') ;
            })

            $('.laporan').click(function(e){
                e.preventDefault();
                window.location = url + '/laporan/' + $(this).data('id') ;
            })
            
            $('.select2').select2();
        </script>
    </body>
</html>