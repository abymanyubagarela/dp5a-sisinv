<header id="topnav" class="print">
    <div class="topbar-main">
        <div class="container-fluid">

            <!-- Logo container-->
            <div class="logo">
                <!-- Text Logo -->
                <a href="#" class="logo">
                    <span class="logo-small"></span>
                    <span class="logo-large">Inventory</span>
                </a>
                <!-- Image Logo -->
                <!-- <a href="index.html" class="logo">
                    <img src="<?= base_url().'assets/front/'?>assets/images/logo_sm.png" alt="" height="26" class="logo-small">
                    <img src="<?= base_url().'assets/front/'?>assets/images/logo.png" alt="" height="16" class="logo-large">
                </a> -->

            </div>
            <!-- End Logo container-->


            <div class="menu-extras topbar-custom print">

                <ul class="list-unstyled topbar-right-menu float-right mb-0">

                    <li class="menu-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle nav-link">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </li>
                    
                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="false" aria-expanded="false">
                            <img src="<?= base_url().'assets/front/'?>assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle"> 
                            <span class="ml-1 pro-user-name"> 
                                <?= ucwords($this->session->userdata['auth']->nik .' - '.$this->session->userdata['auth']->name) ?> 
                                <i class="mdi mdi-chevron-down"></i> 
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                            <!-- item-->
                            <div class="dropdown-item noti-title">
                                <h6 class="text-overflow m-0">Welcome !</h6>
                            </div>

                            <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <i class="fi-head"></i> <span>Profile</span>
                            </a>

                            <!-- item-->
                            <!-- <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <i class="fi-cog"></i> <span>Settings</span>
                            </a> -->

                            <!-- item-->
                            <!-- <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <i class="fi-help"></i> <span>Support</span>
                            </a> -->

                            <!-- item-->
                            <!-- <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <i class="fi-lock"></i> <span>Lock Screen</span>
                            </a> -->

                            <!-- item-->
                            <a href="<?= base_url().'auth/logout'; ?>" class="dropdown-item notify-item">
                                <i class="fi-power"></i> <span>Logout</span>
                            </a>

                        </div>
                    </li>
                </ul>
            </div>
            <!-- end menu-extras -->

            <div class="clearfix"></div>

        </div> <!-- end container -->
    </div>
    <!-- end topbar-main -->

    <div class="navbar-custom">
        <div class="container-fluid">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">

                    <li class="has-submenu">
                        <a href="<?= base_url().'welcome'; ?>"><i class="fi-air-play"></i>Beranda</a>
                    </li>

                    <?php if ($this->session->userdata['auth']->id_role == '1'): ?>
                    <li class="has-submenu">
                        <a href="#"><i class="fi-paper-stack"></i>Master</a>
                        <ul class="submenu">
                            <li>
                                <ul>
                                    <li><a href="<?= base_url().'role'; ?>">Roles</a></li>
                                    <li><a href="<?= base_url().'satuan'; ?>">Satuan</a></li>
                                    <li><a href="<?= base_url().'jenisbarang'; ?>">Jenis Barang</a></li>
                                    <li><a href="<?= base_url().'department'; ?>">Bidang</a></li>
                                    <li><a href="<?= base_url().'kegiatan'; ?>">Kegiatan</a></li>
                                    <li><a href="<?= base_url().'rekening'; ?>">Rekening</a></li>
                                    <li><a href="<?= base_url().'skpd'; ?>">SKPD</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <?php endif ?>

                    <li class="has-submenu">
                        <a href="<?= base_url().'user'; ?>"><i class="fi-head"></i>User</a>
                    </li>
                    
                    <li class="has-submenu">
                        <a href="<?= base_url().'barang'; ?>"><i class="fi-box"></i>Barang</a>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class="fi-paper-clip"></i>Stok</a>
                        <ul class="submenu">
                            <li><a href="<?= base_url().'stok/masuk'; ?>"> Stok Masuk</a></li>                            
                            <li><a href="<?= base_url().'stok/keluar'; ?>"> Stok Keluar</a></li>                            
                        </ul>
                    </li>   

                   <!--  <li>
                        <a href="<?= base_url().'kegiatan'; ?>"> <i class="fi-columns"></i> Kegiatan </a></li>    
                    </li>    -->

                    <li class="has-submenu">
                        <a href="#"><i class="fi-cog"></i>Pengaturan</a>
                        <ul class="submenu">
                            <li><a href="<?= base_url().'user/pengaturan'; ?>"> Pengaturan Kasi & Bendahara</a></li>                              
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class="fi-book"></i>Laporan</a>
                        <ul class="submenu">
                            <li><a href="<?= base_url().'persediaan'; ?>"> Laporan Akhir Persediaan</a></li>                       
                        </ul>
                    </li>                  
                </ul>
                <!-- End navigation menu -->
            </div> <!-- end #navigation -->
        </div> <!-- end container -->
    </div> <!-- end navbar-custom -->
</header>