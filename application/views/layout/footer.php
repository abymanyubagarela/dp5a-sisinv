<!-- Footer -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                2019 - 2020 © DP5A Surabaya. 
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->

<script src="<?= base_url().'assets/front/'?>assets/js/modernizr.min.js"></script>
<!-- jQuery  -->
<script src="<?= base_url().'assets/front/'?>assets/js/jquery.min.js"></script>
<script src="<?= base_url().'assets/front/'?>assets/js/popper.min.js"></script>
<script src="<?= base_url().'assets/front/'?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url().'assets/front/'?>assets/js/waves.js"></script>
<script src="<?= base_url().'assets/front/'?>assets/js/jquery.slimscroll.js"></script>

<!-- Counter number -->
<script src="<?= base_url().'assets/front/'?>assets/plugins/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="<?= base_url().'assets/front/'?>assets/plugins/counterup/jquery.counterup.min.js"></script>

<!-- Chart JS -->
<script src="<?= base_url().'assets/front/'?>assets/plugins/chart.js/chart.bundle.js"></script>

<!-- Required datatable js -->
<script src="<?= base_url().'assets/front/'?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url().'assets/front/'?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Buttons examples -->
<script src="<?= base_url().'assets/front/'?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="<?= base_url().'assets/front/'?>assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="<?= base_url().'assets/front/'?>assets/plugins/datatables/pdfmake.min.js"></script>
<script src="<?= base_url().'assets/front/'?>assets/plugins/datatables/vfs_fonts.js"></script>
<script src="<?= base_url().'assets/front/'?>assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="<?= base_url().'assets/front/'?>assets/plugins/datatables/buttons.print.min.js"></script>

<!-- Responsive examples -->
<script src="<?= base_url().'assets/front/'?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?= base_url().'assets/front/'?>assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

<!-- datepicker -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- App js -->
<script src="<?= base_url().'assets/front/'?>assets/js/jquery.core.js"></script>
<script src="<?= base_url().'assets/front/'?>assets/js/jquery.app.js"></script>

<!-- swal -->
<script src="<?= base_url().'assets/front/'?>assets/plugins/sweet-alert/sweetalert2.min.js"></script>
<!-- swal -->

<!-- select 2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<!-- canvas -->
<script src="<?= base_url().'assets/front/'?>assets/js/signature_pad.umd.js"></script>


<!-- Custom Js here -->
<script type="text/javascript">
	document.body.style.zoom = '95%';
	$(document).ready(function() {
	    
	    var table = $('#datatable-buttons').DataTable({
	        lengthChange: false,
	        ordering: false,
	    });
	});

	$(document).ready(function() {
	    $('#example').DataTable( {
	        
	        buttons: ['print']
	    } );
	} );

    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
</script>

<script type="text/javascript">
	<?php if (!empty($_SESSION['success'])) { ?>
	   swal({
            title: 'Berhasil',
            text : '<?=$_SESSION["success"] ?>',
            type : 'success',
            confirmButtonColor: '#4fa7f3'
        });
	<?php } ?>      

	<?php if (!empty($_SESSION['failed'])) { ?>
		swal({
            title: 'Failed',
            text : '<?=$_SESSION["failed"] ?>',
            type : 'error',
            confirmButtonColor: '#d57171'
        });
	<?php } ?>   
</script>

<!-- alat -->
<script type="text/javascript">
	$('.qrcode').click(function(e){
        e.preventDefault();
        var url_image = "<?= base_url().'qr/'?>" + $(this).data('id') + '/' + $(this).data('id') + '.png' ;
        $(".img-qr").attr("src",url_image);
        $(".qrcodeview").modal('show');                
    })

    $('.lambda').click(function(e){
        e.preventDefault();
        var url = "<?= site_url().'lambda' ?>";
        window.location = url + '/create/' + $(this).data('id') ;
    })

    $('.lambda-view').click(function(e){
        e.preventDefault();
        var url = "<?= site_url().'lambda' ?>";
        window.location = url + '/view/' + $(this).data('id') ;
    })

    $('.reliability').click(function(e){
        e.preventDefault();
        var url = "<?= site_url().'reliability' ?>";
        window.location = url + '/create/' + $(this).data('id') ;
    })

    $('.reliability-view').click(function(e){
        e.preventDefault();
        var url = "<?= site_url().'reliability' ?>";
        window.location = url + '/view/' + $(this).data('id') ;
    })

    $('.rbm').click(function(e){
        e.preventDefault();
        var url = "<?= site_url().'reliability' ?>";
        window.location = url + '/create/' + $(this).data('id') ;
    })

    $('.rbm-view').click(function(e){
        e.preventDefault();
        var url = "<?= site_url().'reliability' ?>";
        window.location = url + '/view/' + $(this).data('id') ;
    })

    $('.failure').click(function(e){
        e.preventDefault();
        var url = "<?= site_url().'failure' ?>";
        window.location = url + '/create/' + $(this).data('id') ;
    })

    $('.failure-view').click(function(e){
        e.preventDefault();
        var url = "<?= site_url().'failure' ?>";
        window.location = url + '/view/' + $(this).data('id') ;
    })
</script>

<!-- End Custom Js here -->            

