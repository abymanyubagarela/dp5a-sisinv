<head>
    <meta charset="utf-8" />
    <title>Sistem Inventory</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="<?= base_url().'assets/front/'?>assets/images/favicon.ico">

     <!-- DataTables -->
    <link href="<?= base_url().'assets/front/'?>assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url().'assets/front/'?>assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="<?= base_url().'assets/front/'?>assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Sweet Alert css -->
        <link href="<?= base_url().'assets/front/'?>assets/plugins/sweet-alert/sweetalert2.min.css" rel="stylesheet" type="text/css" />
        
    <!-- App css -->
    <link href="<?= base_url().'assets/front/'?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url().'assets/front/'?>assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url().'assets/front/'?>assets/css/style.css" rel="stylesheet" type="text/css" />
    <!-- select 2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <style type="text/css">
        .vertical-alignment-helper {
            display:table;
            height: 100%;
            width: 100%;
            pointer-events:none; /* This makes sure that we can still click outside of the modal to close it */
        }
        .vertical-align-center {
            /* To center vertically */
            display: table-cell;
            vertical-align: middle;
            pointer-events:none;
        }
        .modal-content {
            /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */
            width:inherit;
            max-width:inherit; /* For Bootstrap 4 - to avoid the modal window stretching full width */
            height:inherit;
            /* To center horizontally */
            margin: 0 auto;
            pointer-events: all;
        }
    </style>

</head>