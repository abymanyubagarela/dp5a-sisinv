﻿

<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <link type="text/css" media="all" href="https://promo-theme.com/lavr/wp-content/cache/autoptimize/autoptimize_72007150c2cc0fdef62bb1537907a9ce.php" rel="stylesheet" />
    <title>Creative Agency &#8211; Lavr</title>

    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="alternate" type="application/rss+xml" title="Lavr &raquo; Feed" href="https://promo-theme.com/lavr/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Lavr &raquo; Comments Feed" href="https://promo-theme.com/lavr/comments/feed/" />

    <link rel='stylesheet' id='megamenu-css' href='https://promo-theme.com/lavr/wp-content/uploads/maxmegamenu/style.css?ver=fe4c70' type='text/css' media='all' />
    <link rel='stylesheet' id='dashicons-css' href='https://promo-theme.com/lavr/wp-includes/css/dashicons.min.css?ver=4.9.8' type='text/css' media='all' />

    <script type='text/javascript' src='https://promo-theme.com/lavr/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>

    <link rel='https://api.w.org/' href='https://promo-theme.com/lavr/wp-json/' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://promo-theme.com/lavr/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://promo-theme.com/lavr/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 4.9.8" />
    <meta name="generator" content="WooCommerce 3.4.0" />
    <link rel="canonical" href="https://promo-theme.com/lavr/creative-agency/" />
    <link rel='shortlink' href='https://promo-theme.com/lavr/?p=215' />
    <link rel="alternate" type="application/json+oembed" href="https://promo-theme.com/lavr/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fpromo-theme.com%2Flavr%2Fcreative-agency%2F" />
    <link rel="alternate" type="text/xml+oembed" href="https://promo-theme.com/lavr/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fpromo-theme.com%2Flavr%2Fcreative-agency%2F&#038;format=xml" />
    <noscript>
        <style>
            .woocommerce-product-gallery {
                opacity: 1 !important;
            }
        </style>
    </noscript>
    <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." />
    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://promo-theme.com/lavr/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]-->
    <link rel="icon" href="https://promo-theme.com/lavr/wp-content/uploads/2018/04/cropped-logo-black-100x100.png" sizes="32x32" />
    <link rel="icon" href="https://promo-theme.com/lavr/wp-content/uploads/2018/04/cropped-logo-black-300x300.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="https://promo-theme.com/lavr/wp-content/uploads/2018/04/cropped-logo-black-300x300.png" />
    <meta name="msapplication-TileImage" content="https://promo-theme.com/lavr/wp-content/uploads/2018/04/cropped-logo-black-300x300.png" />
    <style type="text/css" data-type="vc_shortcodes-custom-css">
        .vc_custom_1525426232560 {
            padding-top: 14.5% !important;
            padding-bottom: 18.5% !important;
            background-image: url(http://promo-theme.com/lavr/wp-content/uploads/2018/04/bg6.png?id=35) !important;
            background-position: 0 0 !important;
            background-repeat: no-repeat !important;
        }
        
        .vc_custom_1524840306266 {
            padding-top: 20px !important;
            padding-bottom: 33px !important;
        }
        
        .vc_custom_1523956005365 {
            padding-bottom: 15px !important;
            background-color: #f8f8f8 !important;
        }
        
        .vc_custom_1524840336097 {
            padding-top: 15px !important;
        }
        
        .vc_custom_1525426376438 {
            margin-top: 3.1% !important;
            margin-bottom: 6.6% !important;
            padding-top: 20px !important;
            padding-bottom: 8.8% !important;
            background-image: url(http://promo-theme.com/lavr/wp-content/uploads/2018/04/bg.jpg?id=30) !important;
            background-position: center !important;
            background-repeat: no-repeat !important;
            background-size: contain !important;
        }
    </style>
    <noscript>
        <style type="text/css">
            .wpb_animate_when_almost_visible {
                opacity: 1;
            }
        </style>
    </noscript>
</head>

<body class="page-template page-template-template-landing page-template-template-landing-php page page-id-215 woocommerce-no-js mega-menu-navigation header-type-minified header-nav-type-visible_menu header-space-no popup_download_yes project_details_yes site-light wpb-js-composer js-comp-ver-5.4.7 vc_responsive">
    <div id="all" class="site">
        <div class="preloader-area">
            <div class="preloader-folding-cube">
                <div class="preloader-cube1 preloader-cube"></div>
                <div class="preloader-cube2 preloader-cube"></div>
                <div class="preloader-cube4 preloader-cube"></div>
                <div class="preloader-cube3 preloader-cube"></div>
            </div>
        </div>
        <header class="site-header header_minified dark main-row">
            <div class="container">
                <div class="logo">
                    <a href="https://promo-theme.com/lavr/"><img class="light" src="http://promo-theme.com/lavr/wp-content/uploads/2018/04/logo-light.png" alt="Lavr"><img class="dark" src="http://promo-theme.com/lavr/wp-content/uploads/2018/04/logo-black.png" alt="Lavr"></a>
                </div>
                <div class="fr">
                    <div class="minified-block">
                        <nav class="navigation visible_menu">
                            <div id="mega-menu-wrap-navigation" class="mega-menu-wrap">
                                <div class="mega-menu-toggle" tabindex="0">
                                    <div class="mega-toggle-blocks-left"></div>
                                    <div class="mega-toggle-blocks-center"></div>
                                    <div class="mega-toggle-blocks-right">
                                        <div class='mega-toggle-block mega-menu-toggle-block mega-toggle-block-1' id='mega-toggle-block-1'><span class='mega-toggle-label'><span class='mega-toggle-label-closed'>MENU</span><span class='mega-toggle-label-open'>MENU</span></span>
                                        </div>
                                    </div>
                                </div>
                                <ul id="mega-menu-navigation" class="mega-menu max-mega-menu mega-menu-horizontal mega-no-js" data-event="hover_intent" data-effect="fade_up" data-effect-speed="200" data-effect-mobile="disabled" data-effect-speed-mobile="200" data-panel-width="body" data-panel-inner-width=".container" data-second-click="close" data-document-click="collapse" data-vertical-behaviour="accordion" data-breakpoint="768" data-unbind="true">
                                    <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-current-menu-ancestor mega-current-menu-parent mega-menu-item-has-children mega-menu-megamenu mega-align-bottom-left mega-menu-grid mega-menu-item-311' id='mega-menu-item-311'><a class="mega-menu-link" href="#" aria-haspopup="true" tabindex="0">Home</a>
                                        
                                    </li>
                                    <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-323' id='mega-menu-item-323'><a class="mega-menu-link" href="#" aria-haspopup="true" tabindex="0">Pages</a>
                                        
                                    </li>
                                    <li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-330' id='mega-menu-item-330'><a class="mega-menu-link" href="https://promo-theme.com/lavr/blog/" aria-haspopup="true" tabindex="0">Blog</a>
                                        
                                    </li>
                                    <li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-324' id='mega-menu-item-324'><a class="mega-menu-link" href="#" aria-haspopup="true" tabindex="0">Gallery</a>
                                        
                                    </li>
                                    <li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-has-children mega-align-bottom-left mega-menu-flyout mega-menu-item-349' id='mega-menu-item-349'><a class="mega-menu-link" href="https://promo-theme.com/lavr/shop/" aria-haspopup="true" tabindex="0">Shop</a>
                                        
                                    </li>
                                    <li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-item-352' id='mega-menu-item-352'><a class="mega-menu-link" href="https://promo-theme.com/lavr/contacts/" tabindex="0">Contacts</a></li>
                                </ul>
                            </div>
                        </nav>
                        <div class="header-minicart woocommerce header-minicart-lavr">
                            <div class="hm-cunt"><i class="multimedia-icons-shopping-cart"></i><span>0</span></div>
                            <div class="minicart-wrap">

                                <ul class="cart_list product_list_widget ">

                                    <li class="empty">No products in the cart.</li>

                                </ul>
                                <!-- end product list -->

                            </div>
                        </div>
                        <div class="search-button"><i class="base-icons-search"></i></div>
                        <div class="butter-button nav-button hidden_menu hidden">
                            <div></div>
                        </div>
                    </div>
                    <div class="minified-button nav-button">
                        <div class="butter-button">
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="header-space hide"></div>
        <div class="side-bar-area main-row">
            <div class="close base-icons-remove"></div>
            <div class="wrap">
                <div id="black-studio-tinymce-5" class="widget widget_black_studio_tinymce">
                    <h5 class="widget-title">about us</h5>
                    <div class="textwidget">
                        <p><img class="alignnone size-large wp-image-43" src="http://promo-theme.com/lavr/wp-content/uploads/2018/04/img2-1024x685.jpg" alt="" width="1024" height="685" /></p>
                        <p><span style="font-size: 12px; font-weight: 300; line-height: 24px;">As developers, we need to be strategic in how we do this, lest our changes create more headaches down the road for both us and our users. </span></p>
                    </div>
                </div>
                <div class="social-buttons-widget widget">
                    <h5 class="widget-title">follow us</h5>
                    <div class="social-buttons"><a href="#"><i class="fa fa-facebook"></i> <span>Facebook</span></a><a href="#"><i class="fa fa-instagram"></i> <span>Instagram</span></a><a href="#"><i class="fa fa-twitter"></i> <span>Twitter</span></a></div>
                </div>
                <div id="portfolio-2" class="widget widget_portfolio">
                    <h5 class="widget-title">Latest projects</h5>
                    <div class="gallery-module row">
                        <div class="col-xs-4 item">
                            <a href="https://promo-theme.com/lavr/pt-portfolio/lemon/"><img width="150" height="150" src="https://promo-theme.com/lavr/wp-content/uploads/2018/04/img25-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="" title="Lemon" srcset="https://promo-theme.com/lavr/wp-content/uploads/2018/04/img25-150x150.jpg 150w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img25-300x300.jpg 300w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img25-100x100.jpg 100w" sizes="(max-width: 150px) 100vw, 150px" /></a>
                        </div>
                        <div class="col-xs-4 item">
                            <a href="https://promo-theme.com/lavr/pt-portfolio/popcorn/"><img width="150" height="150" src="https://promo-theme.com/lavr/wp-content/uploads/2018/04/img24-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="" title="Popcorn" srcset="https://promo-theme.com/lavr/wp-content/uploads/2018/04/img24-150x150.jpg 150w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img24-300x300.jpg 300w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img24-768x768.jpg 768w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img24-1024x1024.jpg 1024w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img24-600x600.jpg 600w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img24-100x100.jpg 100w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img24.jpg 1272w" sizes="(max-width: 150px) 100vw, 150px" /></a>
                        </div>
                        <div class="col-xs-4 item">
                            <a href="https://promo-theme.com/lavr/pt-portfolio/sweets/"><img width="150" height="150" src="https://promo-theme.com/lavr/wp-content/uploads/2018/04/img23-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="" title="Sweets" srcset="https://promo-theme.com/lavr/wp-content/uploads/2018/04/img23-150x150.jpg 150w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img23-300x300.jpg 300w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img23-100x100.jpg 100w" sizes="(max-width: 150px) 100vw, 150px" /></a>
                        </div>
                        <div class="col-xs-4 item">
                            <a href="https://promo-theme.com/lavr/pt-portfolio/yellow-blank/"><img width="150" height="150" src="https://promo-theme.com/lavr/wp-content/uploads/2018/04/img22-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="" title="Yellow blank" srcset="https://promo-theme.com/lavr/wp-content/uploads/2018/04/img22-150x150.jpg 150w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img22-300x300.jpg 300w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img22-100x100.jpg 100w" sizes="(max-width: 150px) 100vw, 150px" /></a>
                        </div>
                        <div class="col-xs-4 item">
                            <a href="https://promo-theme.com/lavr/pt-portfolio/flowers-2/"><img width="150" height="150" src="https://promo-theme.com/lavr/wp-content/uploads/2018/04/img21-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="" title="Flowers" srcset="https://promo-theme.com/lavr/wp-content/uploads/2018/04/img21-150x150.jpg 150w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img21-300x300.jpg 300w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img21-768x768.jpg 768w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img21-1024x1024.jpg 1024w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img21-600x600.jpg 600w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img21-100x100.jpg 100w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img21.jpg 1268w" sizes="(max-width: 150px) 100vw, 150px" /></a>
                        </div>
                        <div class="col-xs-4 item">
                            <a href="https://promo-theme.com/lavr/pt-portfolio/t-shirt/"><img width="150" height="150" src="https://promo-theme.com/lavr/wp-content/uploads/2018/04/img20-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="" title="T-Shirt" srcset="https://promo-theme.com/lavr/wp-content/uploads/2018/04/img20-150x150.jpg 150w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img20-300x300.jpg 300w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/img20-100x100.jpg 100w" sizes="(max-width: 150px) 100vw, 150px" /></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright">© 2018, LAVR, designed by Promo-Theme.</div>
        </div>
        <div class="search-popup main-row">
            <div class="close base-icons-remove"></div>
            <div class="centered-container">
                <form role="search" method="get" class="searchform" action="https://promo-theme.com/lavr/">
                    <button type="submit" class="searchsubmit" value=""><i class="base-icons-search"></i></button>
                    <div>
                        <input type="text" value="" placeholder="Type and hit enter" name="s" class="input" />
                    </div>
                </form>
            </div>
        </div>

        <main class="main-row">
            <div class="container">

                <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding" style="  ">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner " style="  ">
                            <div class="wpb_wrapper">
                                <div class="banner-area banner-style2 banner-5ad98c49bbbfa">
                                    <div class="social-buttons-text on-side">
                                        <div><a href="#"><i class="fa fa-facebook"></i> <span>Facebook</span></a><a href="#"><i class="fa fa-instagram"></i> <span>Instagram</span></a><a href="#"><i class="fa fa-twitter"></i> <span>Twitter</span></a></div>
                                    </div>
                                    <div class="banner  pagination-style1" style="">
                                        <div class="item banner-item-5b71b214c8868 white content-align-center" style="background-image: url(https://promo-theme.com/lavr/wp-content/uploads/2018/04/slide7.jpg);">
                                            <div class="overlay"></div>
                                            <div class="container">
                                                <div class="cell">
                                                    <h6 class="sub-h"><span>hello there</span></h6>
                                                    <h1 class="h">Design Your<br />Mind<span>.</span></h1>
                                                    <div class="text"><span>The mental side as the ability to see</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item banner-item-5b71b214c8acc white content-align-center" style="background-image: url(https://promo-theme.com/lavr/wp-content/uploads/2018/04/bg7.jpg);">
                                            <div class="overlay"></div>
                                            <div class="container">
                                                <div class="cell">
                                                    <h6 class="sub-h"><span>hello there</span></h6>
                                                    <h1 class="h">Design Your<br />Mind<span>.</span></h1>
                                                    <div class="text"><span>The mental side as the ability to see</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item banner-item-5b71b214c8d0f white content-align-center" style="background-image: url(https://promo-theme.com/lavr/wp-content/uploads/2018/04/slide8.jpg);">
                                            <div class="overlay"></div>
                                            <div class="container">
                                                <div class="cell">
                                                    <h6 class="sub-h"><span>hello there</span></h6>
                                                    <h1 class="h">Design Your<br />Mind<span>.</span></h1>
                                                    <div class="text"><span>The mental side as the ability to see</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vc_row-full-width vc_clearfix"></div>
                <div class="vc_row wpb_row vc_row-fluid vc_custom_1525426232560 vc_row-has-fill custom_color" style="background-position: center center !important;  color: #ffffff;">
                    <div class="wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner " style="  ">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <div style="text-align: center; font-size: 48px; line-height: 1em; font-weight: 600;">We build
                                            <br /> identities
                                            <br /> empowering
                                            <br /> organizations.
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="vc_row wpb_row vc_row-fluid" style="  ">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner " style="  ">
                            <div class="wpb_wrapper">
                                <div class="benefit-items row">
                                    <div class="item col-xs-12 col-sm-6 col-md-4  wpb_animate_when_almost_visible wpb_fadeInLeft fadeInLeft">
                                        <div class="image" style="background-image: url(https://promo-theme.com/lavr/wp-content/uploads/2018/04/img4-1024x688.jpg);">
                                            <a href="#"></a>
                                        </div>
                                        <div class="h">
                                            <div class="cell"><a href="#">Branding<br />identity</a></div>
                                        </div>
                                        <div class="text">We don't just create logos, we help our partners establish an all-encompassing brand experience. Cool factor is not the goal — we develop brand systems that will generate results.</div>
                                    </div>
                                    <div class="item col-xs-12 col-sm-6 col-md-4  wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp">
                                        <div class="image" style="background-image: url(https://promo-theme.com/lavr/wp-content/uploads/2018/04/img2-1024x685.jpg);">
                                            <a href="#"></a>
                                        </div>
                                        <div class="h">
                                            <div class="cell"><a href="#">DISCOVERY<br />&amp; STRATEGY</a></div>
                                        </div>
                                        <div class="text">We don't just create logos, we help our partners establish an all-encompassing brand experience. Cool factor is not the goal — we develop brand systems that will generate results.</div>
                                    </div>
                                    <div class="item col-xs-12 col-sm-6 col-md-4  wpb_animate_when_almost_visible wpb_fadeInRight fadeInRight">
                                        <div class="image" style="background-image: url(https://promo-theme.com/lavr/wp-content/uploads/2018/04/img12-1024x683.jpg);">
                                            <a href="#"></a>
                                        </div>
                                        <div class="h">
                                            <div class="cell"><a href="#">Creative<br />Production</a></div>
                                        </div>
                                        <div class="text">We don't just create logos, we help our partners establish an all-encompassing brand experience. Cool factor is not the goal — we develop brand systems that will generate results.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vc_row wpb_row vc_row-fluid wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp" style="  ">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner " style="  ">
                            <div class="wpb_wrapper">
                                <h2 style="text-align: center" class="h vc_custom_heading heading-decor tac"><span>latest projects</span></h2>
                                <div class="filter-button-group tac">
                                    <div class="wrap">
                                        <button data-filter="*" class="active">All</button>
                                        <button data-filter=".category-art">Art</button>
                                        <button data-filter=".category-design">Design</button>
                                        <button data-filter=".category-lifestyle">Lifestyle</button>
                                        <button data-filter=".category-music">Music</button>
                                        <button data-filter=".category-photography">Photography</button>
                                        <button data-filter=".category-sport">Sport</button>
                                    </div>
                                </div>
                                <div class="portfolio-items row portfolio-type-packery gap-on cols-3 portfolio-5ad9efaaacdff popup-gallery">
                                    <article class="portfolio-item category-art category-photography col-xs-12 col-sm-4 col-md-4">
                                        <div class="wrap ">
                                            <div class="a-img">
                                                <div style="background-image: url(https://promo-theme.com/lavr/wp-content/uploads/2018/04/img1-1024x1024.jpg);"></div>
                                            </div>
                                            <h6><span class="cell">Girl with balls</span></h6></div>
                                        <a href="https://promo-theme.com/lavr/wp-content/uploads/2018/04/img1-1024x1024.jpg" data-size="1024x1024"></a>
                                    </article>
                                    <article class="portfolio-item category-design col-xs-12 col-sm-4 col-md-4">
                                        <div class="wrap ">
                                            <div class="a-img">
                                                <div style="background-image: url(https://promo-theme.com/lavr/wp-content/uploads/2018/04/img2-1024x685.jpg);"></div>
                                            </div>
                                            <h6><span class="cell">Workspace</span></h6></div>
                                        <a href="https://promo-theme.com/lavr/wp-content/uploads/2018/04/img2-1024x685.jpg" data-size="1024x685"></a>
                                    </article>
                                    <article class="portfolio-item category-art category-design col-xs-12 col-sm-4 col-md-4">
                                        <div class="wrap ">
                                            <div class="a-img">
                                                <div style="background-image: url(https://promo-theme.com/lavr/wp-content/uploads/2018/04/img3-799x1024.jpg);"></div>
                                            </div>
                                            <h6><span class="cell">Studio Location</span></h6></div>
                                        <a href="https://promo-theme.com/lavr/wp-content/uploads/2018/04/img3-799x1024.jpg" data-size="799x1024"></a>
                                    </article>
                                    <article class="portfolio-item category-art category-design col-xs-12 col-sm-4 col-md-4">
                                        <div class="wrap ">
                                            <div class="a-img">
                                                <div style="background-image: url(https://promo-theme.com/lavr/wp-content/uploads/2018/04/img4-1024x688.jpg);"></div>
                                            </div>
                                            <h6><span class="cell">Workspace</span></h6></div>
                                        <a href="https://promo-theme.com/lavr/wp-content/uploads/2018/04/img4-1024x688.jpg" data-size="1024x688"></a>
                                    </article>
                                    <article class="portfolio-item category-lifestyle category-photography col-xs-12 col-sm-4 col-md-4">
                                        <div class="wrap ">
                                            <div class="a-img">
                                                <div style="background-image: url(https://promo-theme.com/lavr/wp-content/uploads/2018/04/img5-1024x685.jpg);"></div>
                                            </div>
                                            <h6><span class="cell">Beautiful Girl</span></h6></div>
                                        <a href="https://promo-theme.com/lavr/wp-content/uploads/2018/04/img5-1024x685.jpg" data-size="1024x685"></a>
                                    </article>
                                </div>
                                <div class="load-button tac"><a href="#" data-wrap=".portfolio-5ad9efaaacdff" data-max="5" data-start-page="1" data-next-link="https://promo-theme.com/lavr/creative-agency/page/2/" class="button-style1"><span>Load more</span></a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vc_row wpb_row vc_row-fluid vc_custom_1524840306266" style="  ">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner " style="  ">
                            <div class="wpb_wrapper">
                                <h2 style="text-align: center" class="h vc_custom_heading wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp heading-decor tac"><span>our team</span></h2>
                                <div class="team-items row team-5ad86d0818521 ">
                                    <div class="team-item-style2 item team-item-5b71b214cb3e5 col-xs-12 col-sm-6 col-md-4  wpb_animate_when_almost_visible wpb_fadeInLeft fadeInLeft">
                                        <div class="wrap">
                                            <div class="image">
                                                <div style="background-image: url(https://promo-theme.com/lavr/wp-content/uploads/2018/04/team1.jpg)" class="img"></div>
                                                <div class="team-social-buttons">
                                                    <div><a href="#"><span>Facebook</span></a><a href="#"><span>Instagram</span></a><a href="#"><span>Twitter</span></a></div>
                                                </div>
                                            </div>
                                            <div class="bottom">
                                                <div class="name">Antonina Lane</div>
                                                <div class="post">Artist</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="team-item-style2 item team-item-5b71b214cb5f1 col-xs-12 col-sm-6 col-md-4  wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp">
                                        <div class="wrap">
                                            <div class="image">
                                                <div style="background-image: url(https://promo-theme.com/lavr/wp-content/uploads/2018/04/team2.jpg)" class="img"></div>
                                                <div class="team-social-buttons">
                                                    <div><a href="#"><span>Facebook</span></a><a href="#"><span>Instagram</span></a><a href="#"><span>Twitter</span></a></div>
                                                </div>
                                            </div>
                                            <div class="bottom">
                                                <div class="name">Lisa Watergale</div>
                                                <div class="post">Co-Founder</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="team-item-style2 item team-item-5b71b214cb797 col-xs-12 col-sm-6 col-md-4  wpb_animate_when_almost_visible wpb_fadeInRight fadeInRight">
                                        <div class="wrap">
                                            <div class="image">
                                                <div style="background-image: url(https://promo-theme.com/lavr/wp-content/uploads/2018/04/team3.jpg)" class="img"></div>
                                                <div class="team-social-buttons">
                                                    <div><a href="#"><span>Facebook</span></a><a href="#"><span>Instagram</span></a><a href="#"><span>Twitter</span></a></div>
                                                </div>
                                            </div>
                                            <div class="bottom">
                                                <div class="name">Michael lens</div>
                                                <div class="post">Art-Director</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1523956005365 vc_row-has-fill vc_row-o-equal-height vc_row-o-content-middle vc_row-flex" style="  ">
                    <div class="brand-logo wpb_animate_when_almost_visible wpb_fadeInLeft fadeInLeft wpb_column vc_column_container vc_col-sm-3">
                        <div class="vc_column-inner " style="  ">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <p><img class="alignnone size-full aligncenter" src="http://promo-theme.com/lavr/wp-content/uploads/2018/04/manuf-logo1.png" alt="" width="149" /></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="brand-logo wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp wpb_column vc_column_container vc_col-sm-3">
                        <div class="vc_column-inner " style="  ">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <p><img class="alignnone size-full aligncenter" src="http://promo-theme.com/lavr/wp-content/uploads/2018/04/manuf-logo2.png" alt="" width="150" /></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="brand-logo wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp wpb_column vc_column_container vc_col-sm-3">
                        <div class="vc_column-inner " style="  ">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <p><img class="alignnone size-full aligncenter" src="http://promo-theme.com/lavr/wp-content/uploads/2018/04/manuf-logo3.png" alt="" width="136" /></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="brand-logo last wpb_animate_when_almost_visible wpb_fadeInRight fadeInRight wpb_column vc_column_container vc_col-sm-3">
                        <div class="vc_column-inner " style="  ">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <p><img class="alignnone size-full aligncenter" src="http://promo-theme.com/lavr/wp-content/uploads/2018/04/manuf-logo4.png" alt="" width="149" /></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="vc_row-full-width clearfix" style="height: 100px"></div>

                <div class="vc_row-full-width vc_clearfix">
                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1524837135733" style="">
                       <div class="wpb_column vc_column_container vc_col-sm-12">
                          <div class="vc_column-inner " style="  ">
                             <div class="wpb_wrapper">
                                <div class="woocommerce  wpb_animate_when_almost_visible wpb_fadeInLeft fadeInLeft wpb_start_animation animated">
                                   <div class="products filter-items row">

                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                         <div class="post-112 product type-product status-publish has-post-thumbnail product_cat-shoes first outofstock featured shipping-taxable purchasable product-type-variable has-default-attributes" style="height: 364px;">
                                            <div class="image">
                                                <a rel="nofollow" href="<?php echo site_url().'/detail' ?>" data-quantity="1" data-product_id="112" data-product_sku="woo-vneck-tee" class="button product_type_variable"><span>Select options</span></a>

                                                <a href="<?php echo site_url().'/detail' ?>" class="img">
                                                    <img width="300" height="300" src="https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-300x300.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" srcset="https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-300x300.jpg 300w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-150x150.jpg 150w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-768x768.jpg 768w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-1024x1024.jpg 1024w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-600x600.jpg 600w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-100x100.jpg 100w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1.jpg 1500w" sizes="(max-width: 300px) 100vw, 300px">

                                                    <img width="300" height="300" src="https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-300x300.jpg" class="show" alt="" srcset="https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-300x300.jpg 300w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-150x150.jpg 150w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-768x768.jpg 768w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-1024x1024.jpg 1024w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-600x600.jpg 600w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-100x100.jpg 100w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2.jpg 1600w" sizes="(max-width: 300px) 100vw, 300px">
                                                </a>
                                            </div>
                                            
                                            <a href="<?php echo site_url().'/detail' ?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link" style="height: 102px;">
                                               <div class="name">Shoes</div>
                                               <div class="category">Shoes</div>
                                            </a>
                                         </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                         <div class="post-112 product type-product status-publish has-post-thumbnail product_cat-shoes first outofstock featured shipping-taxable purchasable product-type-variable has-default-attributes" style="height: 364px;">
                                            <div class="image">
                                                <a rel="nofollow" href="<?php echo site_url().'/detail' ?>" data-quantity="1" data-product_id="112" data-product_sku="woo-vneck-tee" class="button product_type_variable"><span>Select options</span></a>

                                                <a href="<?php echo site_url().'/detail' ?>" class="img">
                                                    <img width="300" height="300" src="https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-300x300.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" srcset="https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-300x300.jpg 300w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-150x150.jpg 150w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-768x768.jpg 768w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-1024x1024.jpg 1024w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-600x600.jpg 600w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-100x100.jpg 100w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1.jpg 1500w" sizes="(max-width: 300px) 100vw, 300px">

                                                    <img width="300" height="300" src="https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-300x300.jpg" class="show" alt="" srcset="https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-300x300.jpg 300w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-150x150.jpg 150w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-768x768.jpg 768w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-1024x1024.jpg 1024w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-600x600.jpg 600w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-100x100.jpg 100w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2.jpg 1600w" sizes="(max-width: 300px) 100vw, 300px">
                                                </a>
                                            </div>
                                            
                                            <a href="<?php echo site_url().'/detail' ?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link" style="height: 102px;">
                                               <div class="name">Shoes</div>
                                               <div class="category">Shoes</div>
                                            </a>
                                         </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                         <div class="post-112 product type-product status-publish has-post-thumbnail product_cat-shoes first outofstock featured shipping-taxable purchasable product-type-variable has-default-attributes" style="height: 364px;">
                                            <div class="image">
                                                <a rel="nofollow" href="<?php echo site_url().'/detail' ?>" data-quantity="1" data-product_id="112" data-product_sku="woo-vneck-tee" class="button product_type_variable"><span>Select options</span></a>

                                                <a href="<?php echo site_url().'/detail' ?>" class="img">
                                                    <img width="300" height="300" src="https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-300x300.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" srcset="https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-300x300.jpg 300w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-150x150.jpg 150w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-768x768.jpg 768w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-1024x1024.jpg 1024w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-600x600.jpg 600w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1-100x100.jpg 100w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod1.jpg 1500w" sizes="(max-width: 300px) 100vw, 300px">

                                                    <img width="300" height="300" src="https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-300x300.jpg" class="show" alt="" srcset="https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-300x300.jpg 300w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-150x150.jpg 150w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-768x768.jpg 768w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-1024x1024.jpg 1024w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-600x600.jpg 600w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2-100x100.jpg 100w, https://promo-theme.com/lavr/wp-content/uploads/2018/04/prod2.jpg 1600w" sizes="(max-width: 300px) 100vw, 300px">
                                                </a>
                                            </div>
                                            
                                            <a href="<?php echo site_url().'/detail' ?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link" style="height: 102px;">
                                               <div class="name">Shoes</div>
                                               <div class="category">Shoes</div>
                                            </a>
                                         </div>
                                        </div>

                                   </div>
                                </div>
                             </div>
                          </div>
                       </div>
                    </div>
                </div>

                <div class="vc_row-full-width clearfix" style="height: 100px"></div>

            </div>
        </main>

        <footer class="site-footer light main-row">
            <div class="container">
                <div class="copyright">
                    © 2018, LAVR, Promo-Theme. </div>
                <div class="social-buttons-text">
                    <a href="#"><i class="fa fa-facebook"></i> <span>Facebook</span></a><a href="#"><i class="fa fa-instagram"></i> <span>Instagram</span></a><a href="#"><i class="fa fa-twitter"></i> <span>Twitter</span></a> </div>
            </div>
        </footer>
    </div>



    <script type="text/javascript" defer src="https://promo-theme.com/lavr/wp-content/cache/autoptimize/autoptimize_77757b76a0fae790e2b5e42b1ea2b7fa.php"></script>


    <script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function(event) {
            <?php if (!empty($this->session->userdata['status'])) { ?>
                alert('Pemesanan Berhasil');           

                <?php $this->session->unset_userdata('status'); ?>
            <?php } ?>
        });
        
    
    </script>
</body>

</html>