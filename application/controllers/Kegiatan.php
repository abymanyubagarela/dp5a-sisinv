<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// require('./vendor/autoload.php');

class Kegiatan extends CI_Controller {

	var $data = array();
	function __construct() {
		parent::__construct();
		
		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('auth');
		} 

		## load model here 
		$this->load->model('KegiatanModel', 'Kegiatan');
		$this->load->model('DepartmentModel', 'Department');
		$this->load->model('BarangModel', 'Barang');

		$this->load->model('RekeningModel', 'Rekening');
		$this->load->model('SatuanModel', 'Satuan');
		$this->load->model('BarangModel', 'Barang');

		$this->data = array(
            'controller'	=> 'kegiatan',
            'title' 		=> 'Daftar Kegiatan' ,
            'list'			=> $this->Kegiatan->getAll(),
            'department'	=> $this->Department->getAll(),  
        );
        // $this->reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
	}

	public function index()	{	

		$data = $this->data;

		$this->load->view('inc/kegiatan/list', $data);
	}

	public function insert() {
		
		$err = $this->Kegiatan->insert();
		
		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menambahkan Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menambahkan Data');
		}

		redirect($this->data['controller']);
	}

	/*
	public function insertXls() {
		if($_FILES['upload']['type'] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
			$this->session->set_flashdata('failed', 'File harus bertype XLSX');
		} else {
			$this->reader->setReadDataOnly(true);
			$spreadsheet = $this->reader->load($_FILES['upload']['tmp_name']);
			
			$upload_result_arr = $spreadsheet->getActiveSheet()->toArray();
			
			$kode = array();
			$array_data2 = array();
			if(count($upload_result_arr) > 0) {
				foreach ($upload_result_arr as $index => $key) {
					if($key[0] != ''){
						if(!in_array($key[1], $kode)) {
							array_push($kode, $key[1]);
							$_POST = [];
							$_POST['id_jenis'] = 1;
							$_POST['id'] = $key[0];
							$_POST['id_komponen'] = $key[1];
							$_POST['name'] = $key[2];
							$_POST['note'] = $key[3];
							$_POST['id_satuan'] = $this->Satuan->getByName($key[4]);
							$_POST['harga'] = $key[5];
							$_POST['pajak'] = 10;
							$_POST['id_rekening'] = $this->Rekening->getByKode(explode('(',$key[7])[0]);
							$this->Barang->insert();
						}
					}					
				}
			}
		}		
	}
	*/

	public function edit($id) {
		$data = $this->data;

		$data['list_edit'] = $this->Kegiatan->getByID($id) ;

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}

	public function update() {
		$err = $this->Kegiatan->update($this->input->post('id'));

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Merubah Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Merubah Data');
		}	

		redirect($this->data['controller']);
	}

	public function delete($id) {

		$err = $this->Kegiatan->delete($id);

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menghapus Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menghapus Data, Data Digunakan');
		}	

		redirect($this->data['controller']);
	}
}
