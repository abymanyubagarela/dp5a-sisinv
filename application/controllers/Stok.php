<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok extends CI_Controller {

	var $data = array();
	function __construct() {
		parent::__construct();
		
		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('auth');
		} 

		## load model here 
		$this->load->model('StokModel', 'Stok');
		$this->load->model('BarangModel', 'Barang');

		$this->data = array(
            'controller'=>'stok',
            'barang'=> array(),
            'jenistx' => array('1' => 'masuk' , '2' => 'keluar' )
        );

	}

	public function masuk()	{	

		$data = $this->data;

		$data['title'] = 'Stok Masuk' ;
		$data['jenis'] = '1' ;
		$data['list'] = $this->Stok->getAll($data['jenis']);
		
		$this->load->view('inc/stok/list', $data);
	}

	public function keluar()	{	

		$data = $this->data;

		$data['title'] = 'Stok Keluar' ;
		$data['jenis'] = '2' ;
		$data['list'] = $this->Stok->getAll($data['jenis']);
		$this->load->view('inc/stok/list', $data);
	}

	public function insert() {
		
		$err = $this->Stok->insert();

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menambahkan Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menambahkan Data');
		}

		redirect($this->data['controller'].'/'.$this->data['jenistx'][$this->input->post('jenis')]);
	}

	public function edit($id) {
		$data = $this->data;

		$data['list_edit'] = $this->Stok->getByID($id) ;

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}

	public function update() {
		$err = $this->Stok->update($this->input->post('id'));

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Merubah Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Merubah Data');
		}	

		redirect($this->data['controller'].'/'.$this->data['jenistx'][$this->input->post('jenis')]);
	}

	public function delete($id) {
		$err = $this->Stok->delete($id);

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menghapus Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menghapus Data, Data Digunakan');
		}	

		$detail = $this->Stok->getByID($id);

		redirect($this->data['controller'].'/'.$this->data['jenistx'][$detail->jenis]);
	}

	public function getbarang() {
		$data = $this->data;

		$key = $this->input->post('search') ;

		$data['barang'] = $this->Barang->getByKey($key) ;

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data['barang']));

	    return $data['barang'];
	}
}
