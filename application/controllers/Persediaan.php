<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persediaan extends CI_Controller {

	var $data = array();
	function __construct() {
		parent::__construct();
		
		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('auth');
		} 

		## load model here 
		$this->load->model('PersediaanModel', 'Persediaan');
		$this->load->model('KegiatanModel', 'Kegiatan');
		$this->load->model('SkpdModel', 'Skpd');
		$this->load->model('StokModel', 'Stok');
		$this->load->model('BarangModel', 'Barang');

		$this->data = array(
            'controller'=>'persediaan',
            'kegiatan' => $this->Kegiatan->getList(),
            'skpd' => $this->Skpd->getList(),
            'barang' => array(),
        );
	}

	public function index()	{	

		$data = $this->data;

		$data['title'] = 'Laporan Akhir Persediaan' ;

		$data['list'] = $this->Persediaan->getAll();
		
		$data['column'] = $this->Persediaan->getColumn();	

		$this->load->view('inc/persediaan/list', $data);
	}

	public function insert() {
		
		$err = $this->Persediaan->insert();

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menambahkan Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menambahkan Data');
		}

		redirect($this->data['controller']);
	}

	public function edit($id) {
		$data = $this->data;

		$data['list_edit'] = $this->Persediaan->getByID($id) ;

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}

	public function update() {
		$err = $this->Persediaan->update($this->input->post('id'));

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Merubah Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Merubah Data');
		}	

		redirect($this->data['controller']);
	}

	public function delete($id) {
		$err = $this->Persediaan->delete($id);

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menghapus Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menghapus Data, Data Digunakan');
		}	

		redirect($this->data['controller']);
	}

	public function detail($id) {
		$data = $this->data;
		
		$data['total'] = $this->Stok->getNilaiAkhir($id);

	    $this->Persediaan->updateNilaiAkhir($id, $data['total']) ;

	    $data['title'] = 'Daftar Barang Per Kegiatan' ;

	    $data['list_edit'] = $this->Persediaan->getByDetail($id) ;

		$data['list'] = $this->Stok->getListPerKegiatan($id);
		
		$this->load->view('inc/persediaan/detail', $data);
	}

	public function laporan($id) {
		$data = $this->data;
		
		$data['total'] = $this->Stok->getNilaiAkhir($id);

	    $data['title'] = 'Laporan Akhir Persediaan Per Kegiatan' ;

	    $data['list_edit'] = $this->Persediaan->getByDetail($id) ;

		$data['list'] = $this->Stok->getListPerKegiatan($id);

		$id_rekening = array();
		foreach ($data['list'] as $index => $key) {
			
			if(!in_array($key->id_rekening, $id_rekening)) {
				$detail[$key->id_rekening]['judul'] = $key->id_rekening_description;
				$detail[$key->id_rekening]['data'][] = $key;
			}
		}
		
		$data['list'] = $detail;
		// print_r($data['list']);die();
		$this->load->view('inc/persediaan/laporan', $data);
	}

	public function insertdetail() {
		
		$err = $this->Stok->insert();

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menambahkan Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menambahkan Data');
		}

		redirect($this->data['controller'].'/detail/'.$this->input->post('id_kegiatan'));
	}

	public function editdetail($id) {
		$data = $this->data;

		$data['list_edit'] = $this->Stok->getByID($id) ;

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}

	public function updatedetail() {
		
		$err = $this->Stok->update($this->input->post('id'));

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Merubah Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Merubah Data');
		}	


		$detail = $this->Persediaan->getByID($id) ;

		redirect($this->data['controller'].'/detail/'.$this->input->post('id_kegiatan'));
	}

	public function deletedetail($id) {
		$err = $this->Stok->delete($id);

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menghapus Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menghapus Data, Data Digunakan');
		}	

		$detail = $this->Stok->getByID($id);

		redirect($this->data['controller'].'/detail/'.$detail->id_kegiatan);
	}

	public function getbarang() {
		$data = $this->data;

		$key = $this->input->post('search') ;

		$data['barang'] = $this->Barang->getByKey($key) ;

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data['barang']));

	    return $data['barang'];
	}

	public function getlastsaldo() {
		
		$key = $this->input->post('id_barang') ;

		$data['barang'] = $this->Stok->getLastSaldo($key) ;

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data['barang']));

	    return $data['barang'];
	}

	public function sendsignature() {
		
		$data['err'] = $this->Persediaan->saveSignature() ;

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data['err']));

	    return $data;
	}
}
