<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	var $data = array();
	function __construct() {
		parent::__construct();
		
		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('auth');
		} 

		$this->data = array(
            'controller'=>'user'
        );

		## load model here 
		$this->load->model('UserModel', 'User');
		$this->load->model('DepartmentModel', 'Department');
		$this->load->model('RoleModel', 'Role');
	}

	public function index()	{	

		$data = $this->data;

		$data['title'] = 'User' ;
		$data['list'] = $this->User->getAll();
		$data['column'] = $this->User->getColumn();	

		$data['department'] = $this->Department->getList();
		$data['role'] = $this->Role->getList();

		$this->load->view('inc/user/list', $data);
	}

	public function insert() {

		$err = $this->User->insert();

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menambahkan Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menambahkan Data');
		}

		redirect($this->data['controller']);
	}

	public function edit($id) {
		$data = $this->data;

		$data['list_edit'] = $this->User->getByID($id) ;

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}

	public function update() {
		$err = $this->User->update($this->input->post('id'));

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Merubah Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Merubah Data');
		}	

		redirect($this->data['controller']);
	}

	public function delete($id) {
		$err = $this->User->delete($id);

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menghapus Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menghapus Data, Data Digunakan');
		}	

		redirect($this->data['controller']);
	}

	public function pengaturan()	{	
		$data = $this->data;
		$data['title'] = 'Pengaturan User' ;
		$data['list'] = $this->User->getUserIncharge();
		$data['notdepartment'] = $this->Department->getListNotInclude($this->User->getUserBidang());
		$data['department'] = $this->Department->getList();
		
		$this->load->view('inc/user/list-pengaturan', $data);
	}

	public function updatepengaturan() {
		$err = $this->User->updatepengaturan();

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Merubah Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Merubah Data');
		}	

		redirect($this->data['controller'].'/pengaturan');
	}

	public function updatepengaturanedit() {
		$err = $this->User->updatepengaturanedit();
		
		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Merubah Data');
		} else {
			if ($err['code'] == '100') {
				$this->session->set_flashdata('failed', $err['message']);	
			} else {
				$this->session->set_flashdata('failed', 'Gagal Merubah Data');
			}
		}	

		redirect($this->data['controller'].'/pengaturan');
	}

	public function getlistbybidang($id) {
		$data['list_bidang'] = $this->User->getListByBidang($id) ;

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}
}
