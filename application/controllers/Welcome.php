<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
		parent::__construct();

		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('auth');
		} 

		$this->load->model('DepartmentModel', 'Department');

		$this->data = array(
            'controller'=>'welcome'
        );
	}

	public function index()	{	
		$data = $this->data;

		$this->load->view('inc/dashboard',$data);		
	}	
}
