<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct() {
		parent::__construct();
		
		$this->load->model('UserModel', 'User');
		
		$this->data = array(
            'controller'=>'auth'
        );
	}

	public function index()	{	
		$data = $this->data;

		// $this->load->view('inc/login',$data);
		$this->load->view('login/index',$data);		
	}

	public function loginpost()	{	

		$data = $this->data;
		$exist = $this->User->getLogin();
		
		if (empty($exist)) {				
			$this->session->set_flashdata('failed', 'User Not Found');
			redirect($this->data['controller']);		
		} else {
			$this->session->set_userdata('auth', $exist);		
			$this->session->set_flashdata('success', 'Selamat Datang');
			redirect('welcome');		
		}
		$this->load->view('inc/login', $data);
	}

	public function logout()	{	

		$this->session->sess_destroy();

		redirect($this->data['controller']);		
	}

}
