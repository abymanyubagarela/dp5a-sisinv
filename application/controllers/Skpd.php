<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skpd extends CI_Controller {

	var $data = array();
	function __construct() {
		parent::__construct();
		
		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('auth');
		} 

		$this->data = array(
            'controller'=>'skpd'
        );

        
		## load model here 
		$this->load->model('SkpdModel', 'Skpd');
	}

	public function index()	{	

		$data = $this->data;

		$data['title'] = 'SKPD' ;
		$data['list'] = $this->Skpd->getAll();
		$data['column'] = $this->Skpd->getColumn();	

		$this->load->view('inc/skpd/list', $data);
	}

	public function insert() {
		
		$err = $this->Skpd->insert();

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menambahkan Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menambahkan Data');
		}

		redirect($this->data['controller']);
	}

	public function edit($id) {
		$data = $this->data;

		$data['list_edit'] = $this->Skpd->getByID($id) ;

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}

	public function update() {
		$err = $this->Skpd->update($this->input->post('id'));

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Merubah Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Merubah Data');
		}	

		redirect($this->data['controller']);
	}

	public function delete($id) {
		$err = $this->Skpd->delete($id);

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menghapus Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menghapus Data, Data Digunakan');
		}	

		redirect($this->data['controller']);
	}
}
