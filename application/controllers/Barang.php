<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	var $data = array();
	function __construct() {
		parent::__construct();
		
		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('auth');
		} 

		## load model here 
		$this->load->model('BarangModel', 'Barang');
		$this->load->model('SatuanModel', 'Satuan');
		$this->load->model('JenisBarangModel', 'Jenis');
		$this->load->model('RekeningModel', 'Rekening');

		$this->data = array(
            'controller'=>	'barang',
            'satuan'	=>  $this->Satuan->getList(),
            'jenis'		=>  $this->Jenis->getList(),
            'rekening'		=>  $this->Rekening->getList(),
        );
	}

	public function index()	{	

		$data = $this->data;

		$data['title'] = 'Barang' ;
		$data['list'] = $this->Barang->getAll();
		$data['column'] = $this->Jenis->getColumn();	
		
		$this->load->view('inc/barang/list', $data);
	}

	public function insert() {

		$err = $this->Barang->insert();

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menambahkan Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menambahkan Data');
		}

		redirect($this->data['controller']);
	}

	public function edit($id) {
		$data = $this->data;

		$data['list_edit'] = $this->Barang->getByID($id) ;

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}

	public function update() {
		$err = $this->Barang->update($this->input->post('id'));

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Merubah Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Merubah Data');
		}	

		redirect($this->data['controller']);
	}

	public function delete($id) {
		$err = $this->Barang->delete($id);

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menghapus Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menghapus Data, Data Digunakan');
		}	

		redirect($this->data['controller']);
	}

	function get_data()
    {
    	$list = $this->Barang->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->id_komponen;
            $row[] = $field->name;
            $row[] = $field->id_jenis;
            $row[] = $field->jumlah;
            $row[] = $field->id_satuan;
            $row[] = $field->harga;
            $row[] = $field->pajak;
            $row[] = $field->id_rekening;
            $row[] = $field->note;
            $row[] = 
            '<button type="button" class="btn btn-icon waves-effect waves-light btn-warning btn-xs edit mr-2" onclick="edit('.$field->id.')"> <i class="fa fa-wrench"></i> </button>'.
            '<button type="button" class="btn btn-icon waves-effect waves-light btn-danger btn-xs delete" onclick="delet('.$field->id.')"> <i class="fa fa-remove"></i> </button>';
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Barang->count_all(),
            "recordsFiltered" => $this->Barang->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
}
