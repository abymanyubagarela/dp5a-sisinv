<?php  

	class PersediaanModel extends CI_Model
	{
		public function __construct() {
			parent::__construct();

	        ## declate table name here
	        $this->table_name = 'data_persediaan_kegiatan' ;
	    }

	    ## get all data in table
	    function getAll() {
	    	$this->db->select('
				data_persediaan_kegiatan.*,
				m_skpd.name as id_skpd,
				m_kegiatan.description as id_kegiatan,
				'
	    	);
	    	$this->db->join('m_skpd', 'm_skpd.id = data_persediaan_kegiatan.id_skpd', 'left'); 
	    	$this->db->join('m_kegiatan', 'm_kegiatan.id = data_persediaan_kegiatan.id_kegiatan', 'left'); 
	    	$this->db->where('data_persediaan_kegiatan.is_active','1');

	    	if($this->session->userdata['auth']->id_role != '1') {
				$this->db->where('data_persediaan_kegiatan.id_department',$this->session->userdata['auth']->id_department);	    		
	    	}

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		## get all data in table for list (select)
	    function getList() {
	    	
	    	$this->db->select('data_persediaan_kegiatan.id, data_persediaan_kegiatan.name');

	    	$this->db->where(array('is_active' => '1'));

	    	if($this->session->userdata['auth']->id_role != '1') {
				$this->db->where('data_persediaan_kegiatan.id_department',$this->session->userdata['auth']->id_department);	    		
	    	}

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		## get data by id in table
	    function getByID($id) {
	        $this->db->where(array('id' => $id));
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->row();
	    }

	    ## get data by id in table
	    function getByDetail($id) {
	        $this->db->select('
				data_persediaan_kegiatan.*,
				concat(m_skpd.kode," - ",m_skpd.name) as id_skpd,
				concat(m_kegiatan.kode," - ",m_kegiatan.description)  as id_kegiatan_deskripsi,
				concat(kasi.nik," - ",kasi.name)  as kasi,
				concat(bendahara.nik," - ",bendahara.name)  as bendahara'
	    	);
	    	$this->db->join('m_skpd', 'm_skpd.id = data_persediaan_kegiatan.id_skpd', 'left'); 
	    	$this->db->join('m_kegiatan', 'm_kegiatan.id = data_persediaan_kegiatan.id_kegiatan', 'left'); 
	    	$this->db->join('data_user kasi', 'kasi.id = data_persediaan_kegiatan.id_kasie', 'left'); 
	    	$this->db->join('data_user bendahara', 'bendahara.id = data_persediaan_kegiatan.id_bendahara', 'left'); 

	        $this->db->where(array('data_persediaan_kegiatan.id' => $id));
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->row();
	    }

	    ## get column name in table
	    function getColumn() {

	        return $this->db->list_fields($this->table_name);
	    }

	    ## insert data into table
	    function insert() {
	        $a_input = array();
	       
	        foreach ($_POST as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $a_input['date_created'] = date('Y-m-d H:m:s');
	        $a_input['is_active']	 = '1';
	        $a_input['id_department'] = $this->session->userdata['auth']->id_department;
	        
	        $this->db->insert($this->table_name, $a_input);

	        return $this->db->error();	        
	    }

	    ## update data in table
	    function update($id) {
	    	$_data = $this->input->post() ;
	    	
	        foreach ($_data as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $a_input['date_updated'] = date('Y-m-d H:m:s');	        

	        $this->db->where('id', $id);
	        
	        $this->db->update($this->table_name, $a_input);

	        return $this->db->error();	        
	    }

	    ## delete data in table
		function delete($id) {
			$a_input['is_active'] = '0';    
			
			$this->db->where('id', $id);

			$this->db->update($this->table_name, $a_input);

			return $this->db->error();	      
		}

		function saveSignature(){
			$data = $this->input->post() ;
	    	
			if ($data['type'] == '1') {
				$a_input['kasie_ttd'] = $data['sign'];
		        $a_input['id_kasie'] = $this->session->userdata['auth']->id;
			} else {
				$a_input['bendahara_ttd'] = $data['sign'];
		        $a_input['id_bendahara'] = $this->session->userdata['auth']->id;
			}

	        $this->db->where('id', $data['id']);	        

	        $this->db->update($this->table_name, $a_input);

	        return $this->db->error();	

		}

		function updateNilaiAkhir($id, $nilai) {

			$a_input['saldo_akhir'] = $nilai;	        

	        $this->db->where('id', $id);
	        
	        $this->db->update($this->table_name, $a_input);

	        return $this->db->error();	 
		}
	}

?>