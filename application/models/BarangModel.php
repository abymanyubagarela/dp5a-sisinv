<?php  

	class BaraNgmodel extends CI_Model
	{
		var $table = 'data_barang'; 
	    var $column_order = array(null, 'data_barang.id_komponen','data_barang.name','data_barang.harga','id_jenis','id_satuan','id_rekening','pajak','note','id');
	    var $column_search = array('data_barang.id_komponen','data_barang.name');
	    var $order = array('data_barang.id' => 'asc'); 

		public function __construct() {
			parent::__construct();

			$this->load->database();
	        
	        ## declate table name here
	        $this->table_name = 'data_barang' ;
	    }

	    ## get all data in table
	    function getAll() {
	    	$this->db->select('
				data_barang.id,
				data_barang.id_komponen,
	    		data_barang.name,
	    		data_barang.harga,
	    		data_barang.jumlah,
	    		data_barang.pajak,
	    		data_barang.note,
	    		m_satuan.name as id_satuan,
	    		m_jenis_barang.name as id_jenis,
	    		m_rekening.description as id_rekening,'
	    	);

	    	$this->db->join('m_satuan', 'm_satuan.id = data_barang.id_satuan', 'left'); 
	    	$this->db->join('m_jenis_barang', 'm_jenis_barang.id = data_barang.id_jenis', 'left'); 
	    	$this->db->join('m_rekening', 'm_rekening.id = data_barang.id_rekening', 'left'); 
	    	
	    	$this->db->where('data_barang.is_active','1');

	    	$query = $this->db->get($this->table_name);

	        return $query->result();
		}

		## get all data in table for list (select)
	    function getList() {
	    	
	    	$this->db->select('data_barang.id, data_barang.id_komponen, data_barang.name');

	    	$this->db->where(array('is_active' => '1'));

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		## get data by id in table
	    function getByID($id) {
	        $this->db->where('id', $id);
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->row();
	    }

	    ## get data by keyword in table
	    function getByKey($keyword) {
	        $this->db->where('is_active', 1);
	        
	        $this->db->where("name like '%".$keyword."%' ");
	        $this->db->or_where("id_komponen like '%".$keyword."%' ");

	        $query = $this->db->get($this->table_name);
	        
	        $result = $query->result_array();

	        $data = array();
	
			foreach($result as $res){
				$data[] = array('id' => $res['id'], 'text' => $res['id_komponen'] . ' - ' .$res['name']);
			}
	    
		    return $data;
	    }

	    ## get column name in table
	    function getColumn() {

	        return $this->db->list_fields($this->table_name);
	    }

	    ## insert data into table
	    function insert() {
	        $a_input = array();
	       
	        foreach ($_POST as $key => $row) {
	            $a_input[$key] = $row;	
	        }

	        $a_input['name']	 	 = ucwords($a_input['name']);
	        $a_input['date_created'] = date('Y-m-d H:m:s');
	        $a_input['is_active']	 = '1';
	        
	        $this->db->insert($this->table_name, $a_input);
	        
	        return $this->db->error();	        
	    }

	    ## update data in table
	    function update($id,$data=null) {
	    	if($data) {
	    		$_data = $data ;	
	    	} else {
	    		$_data = $this->input->post() ;	
	    	}	    	
	    	
	        foreach ($_data as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        unset($a_input['id']);
	        $a_input['date_updated'] = date('Y-m-d H:m:s');	  
	        
	        $this->db->where('id', $id);
	        
	        $this->db->update($this->table_name, $a_input);

	        return $this->db->error();	        
	    }

	    ## delete data in table
		function delete($id) {
			$a_input['is_active'] = '0';    
			
			$this->db->where('id', $id);

			$this->db->update($this->table_name, $a_input);

			return $this->db->error();	      
		}

		## server side ##
		private function _get_datatables_query()
	    {
	    	$this->db->select('
				data_barang.id,
				data_barang.id_komponen,
	    		data_barang.name,
	    		data_barang.harga,
	    		data_barang.jumlah,
	    		data_barang.pajak,
	    		data_barang.note,
	    		m_satuan.name as id_satuan,
	    		m_jenis_barang.name as id_jenis,
	    		m_rekening.description as id_rekening,'
	    	);

	    	$this->db->join('m_satuan', 'm_satuan.id = data_barang.id_satuan', 'left'); 
	    	$this->db->join('m_jenis_barang', 'm_jenis_barang.id = data_barang.id_jenis', 'left'); 
	    	$this->db->join('m_rekening', 'm_rekening.id = data_barang.id_rekening', 'left'); 
	    	
	    	$this->db->where('data_barang.is_active','1');
	         
	        $this->db->from($this->table);
	 
	        $i = 0;
	     
	        foreach ($this->column_search as $item) // looping awal
	        {
	            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
	            {
	                 
	                if($i===0) // looping awal
	                {
	                    $this->db->group_start(); 
	                    $this->db->like($item, $_POST['search']['value']);
	                }
	                else
	                {
	                    $this->db->or_like($item, $_POST['search']['value']);
	                }
	 
	                if(count($this->column_search) - 1 == $i) 
	                    $this->db->group_end(); 
	            }
	            $i++;
	        }
	         
	        if(isset($_POST['order'])) 
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    function get_datatables()
	    {
	        $this->_get_datatables_query();
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    function count_filtered()
	    {
	        $this->_get_datatables_query();
	        $query = $this->db->get();
	        return $query->num_rows();
	    }
	 
	    public function count_all()
	    {
	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }
	    ## server side ##	
	}

?>