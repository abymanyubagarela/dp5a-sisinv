<?php  

	class StokModel extends CI_Model
	{
		public function __construct() {
			parent::__construct();

	        ## declate table name here
	        $this->table_name = 'data_stok_barang' ;
	        $this->load->model('BarangModel', 'Barang');
	    }

	    ## get all data in table
	    function getAll($jenis) {
	    	// print_r($this->session->userdata['auth']->id_department);die();
	    	$this->db->select('
				data_stok_barang.*,
				data_barang.id_komponen,
	    		data_barang.name'
	    	);
	    	
	    	$this->db->join('data_barang', 'data_barang.id = data_stok_barang.id_barang', 'left'); 
	    	$this->db->join('data_user', 'data_user.id = data_stok_barang.id_barang', 'left'); 

	    	$this->db->where(
	    		array(
	    			'data_stok_barang.is_active'=>'1', 
	    			'data_stok_barang.jenis' => $jenis,
	    			'data_stok_barang.id_department' => $this->session->userdata['auth']->id_department
	    		)
	    	);

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		## get data by id in table
	    function getByID($id) {
	    	$this->db->select('
				data_stok_barang.*,
				data_barang.id_komponen,
				data_barang.name'
	    	);

	        $this->db->where(array('data_stok_barang.id' => $id));
	        
	        $this->db->join('data_barang', 'data_barang.id = data_stok_barang.id_barang', 'left'); 

	        $query = $this->db->get($this->table_name);
	        
	        return $query->row();
	    }

	    ## get column name in table
	    function getColumn() {

	        return $this->db->list_fields($this->table_name);
	    }

	    ## insert data into table
	    function insert() {
	    	
	        $a_input = array();
	       
	        foreach ($_POST as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $a_input['date_created'] = date('Y-m-d H:m:s');
	        $a_input['is_active']	 = '1';
	        $a_input['created_by']	 = $this->session->userdata['auth']->id;
	        $a_input['id_department']	 = $this->session->userdata['auth']->id_department;
	        
	        $this->db->insert($this->table_name, $a_input);

	        $this->insertstokbarang($a_input);

	        return $this->db->error();	        
	    }

	    ## update data in table
	    function update($id) {
	    	$_data = $this->input->post() ;
	    	
	        foreach ($_data as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        unset($a_input['jumlahx']);

	        $a_input['date_updated'] = date('Y-m-d H:m:s');	        

	        $a_input['created_by']	 = $this->session->userdata['auth']->id;

	        $this->db->where('id', $id);
			
			$this->db->update($this->table_name, $a_input);

	        $this->updatestokbarang($_data);

	        return $this->db->error();	        
	    }

	    ## delete data in table
		function delete($id) {
			$a_input['is_active'] = '0';   

			$this->db->where('id', $id);
			
			$this->db->update($this->table_name, $a_input);

			$a_input['id_barang'] = $id;    

			$this->deletestokbarang($a_input);

			return $this->db->error();	      
		}

		function insertstokbarang($data) {
			$detail = $this->Barang->getByID($data['id_barang']);
			
			if($data['jenis'] == '1') {
				$data['jumlah'] += $detail->jumlah;
			} else {
				$data['jumlah'] = $detail->jumlah - $data['jumlah'];
			}
			
			$a_input['id'] = $data['id_barang'];   
			
			$a_input['jumlah'] = $data['jumlah'];   

			$this->Barang->update($a_input['id'],$a_input);  
		}

		function updatestokbarang($data) {

			$detail = $this->Barang->getByID($data['id_barang']);
			
			if($data['jenis'] == '1') {
				$a_input['jumlah'] = $detail->jumlah - $data['jumlahx'] + $data['jumlah'];   
			} else {
				$a_input['jumlah'] = $detail->jumlah + $data['jumlahx'] - $data['jumlah'];   
			}
			
			$a_input['id'] = $data['id_barang'];   
			
			$this->Barang->update($a_input['id'],$a_input);  
		}

		function deletestokbarang($data) {
			$detail_stok = $this->getByID($data['id_barang']);
			
			$detail_barang = $this->Barang->getByID($detail_stok->id_barang);
			
			$data['jumlah'] = $detail_barang->jumlah;
			
			if($detail_stok->jenis == '1') {
				$data['jumlah'] -= $detail_stok->jumlah;
			} else {
				$data['jumlah'] += $detail_stok->jumlah;
			}
			
			$a_input['id'] = $data['id_barang'];   
			
			$a_input['jumlah'] = $data['jumlah'];   

			$this->Barang->update($detail_barang->id,$a_input);  
		}

		## get all data in table
	    function getListPerKegiatan($idkegiatan) {
	    	$this->db->select('
				data_stok_barang.*,
				sum(data_stok_barang.jumlah) as total,
				data_barang.id_komponen,
	    		data_barang.id_jenis,
	    		data_barang.name,
	    		data_barang.harga,
	    		data_barang.pajak,
	    		data_barang.jumlah as jumlah_barang,
	    		m_rekening.id as id_rekening,
	    		m_rekening.description as description_rekening,
	    		concat(m_rekening.kode," - ", m_rekening.description) as id_rekening_description'
	    	);
	    	
	    	$this->db->join('data_barang', 'data_barang.id = data_stok_barang.id_barang', 'left'); 
	    	$this->db->join('m_rekening', 'm_rekening.id = data_barang.id_rekening', 'left'); 

	    	$this->db->where(
	    		array(
		    		'data_stok_barang.is_active'=> '1', 
		    		'data_stok_barang.jenis' => '2',
		    		'data_stok_barang.id_kegiatan'=> $idkegiatan, 
	    		)
	    	);

	    	$this->db->group_by(array('data_stok_barang.id_barang'));  

	    	$this->db->order_by('data_stok_barang.id');  

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		## get total pengeluaran
	    function getNilaiAkhir($id) {
	        $this->db->select('data_stok_barang.jumlah, data_barang.pajak, data_barang.harga');

	    	$this->db->join('data_barang', 'data_barang.id = data_stok_barang.id_barang', 'left'); 

	        $this->db->where(
	        	array(
	        		'data_stok_barang.is_active' => 1,
	        		'data_stok_barang.jenis' => 2,
	        		'data_stok_barang.id_kegiatan' => $id,
	        	)
	        );
	        
	        $query = $this->db->get($this->table_name);
	        
	        $data = $query->result();

	        $total = 0 ;
	        
	        foreach ($data as $index => $key) {
	        	$total += ( $key->jumlah * $key->harga ) + ( $key->pajak/100 * ($key->jumlah * $key->harga) ); 
	        }

	        return $total;
	    }

	    function getLastSaldo($id) {
	    	$stok_masuk = 0 ;
	    	$stok_keluar = 0 ;
			$this->db->select_sum('jumlah', 'masuk');
	        $this->db->where(
	        	array(
	        		'is_active' => 1,
	        		'id_barang' => $id,
	        		'jenis' => 1,
	        		'id_department' => $this->session->userdata['auth']->id_department
	        	)
	        );
	        
	        $query = $this->db->get($this->table_name);
	        $stok_masuk = $query->row()->masuk;

	        $this->db->select_sum('jumlah', 'keluar');
	        $this->db->where(
	        	array(
	        		'is_active' => 1,
	        		'id_barang' => $id,
	        		'jenis' => 2,
	        		'id_department' => $this->session->userdata['auth']->id_department
	        	)
	        );
	        
	        $query = $this->db->get($this->table_name);
	        $stok_keluar = $query->row()->keluar;
	       	
			$data['jumlah'] = $stok_masuk - $stok_keluar;
			$data['masuk'] = $stok_masuk;
			$data['keluar'] =$stok_keluar;
	        return $data;
	    }
	}

?>		