<?php  

	class UserModel extends CI_Model
	{
		public function __construct() {
			parent::__construct();

	        ## declate table name here
	        $this->table_name = 'data_user' ;
	    }

	    ## get all data in table
	    function getAll() {
	    	
	    	$this->db->select('data_user.*, m_role.name as id_role, m_department.name as id_department');
	    	$this->db->join('m_role', 'm_role.id = data_user.id_role', 'left');  
	    	$this->db->join('m_department', 'm_department.id = data_user.id_department', 'left');  
	    	$this->db->where(
	    		array(
	    			'data_user.is_active' => '1',
	    			'data_user.id_role !=' => '1'
	    		)
	    	);

	    	if($this->session->userdata['auth']->id_role != '1') {
				$this->db->where('data_user.id_department',$this->session->userdata['auth']->id_department);	    		
	    	}

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		## get all data in table for list (select)
	    function getList() {
	    	
	    	$this->db->select('data_user.id, data_user.name');
	    	$this->db->where(array('is_active' => '1'));

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		## get all data in table for list (select) by id
	    function getListByBidang($id_department) {
	    	
	    	$this->db->select('data_user.id, data_user.name');
	    	$this->db->where(array('is_active' => '1','id_department' => $id_department));

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		## get data by id in table
	    function getByID($id) {
	        $this->db->where(array('id' => $id));
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->row();
	    }

	    ## get data by id_role in table
	    function getByRole($id) {
	    	$this->db->select('data_user.id, data_user.name');
	        $this->db->where(array('id_role' => $id));
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->result();
	    }

	    ## get data by name in table
	    function getByName($id) {
	    	$this->db->select('data_user.name');
	        $this->db->where(array('id' => $id));
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->row();
	    }

	    ## get column name in table
	    function getColumn() {

	        return $this->db->list_fields($this->table_name);
	    }

	    ## insert data into table
	    function insert() {
	        $a_input = array();
	       
	        foreach ($_POST as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $a_input['date_created'] = date('Y-m-d H:m:s');
	        $a_input['is_active']	 = '1';
	        
	        unset($a_input['type']);
	        $this->db->insert($this->table_name, $a_input);

	        return $this->db->error();	        
	    }

	    ## update data in table
	    function update($id) {
	    	$_data = $this->input->post() ;
	    	
	        foreach ($_data as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $a_input['date_updated'] = date('Y-m-d H:m:s');	        

	        unset($a_input['type']);
	        
	        $this->db->where('id', $id);
	        $this->db->update($this->table_name, $a_input);

	        return $this->db->error();	        
	    }

	    ## delete data in table
		function delete($id) {
			$a_input['is_active'] = '0';    
			
			$this->db->where('id', $id);

			$this->db->update($this->table_name, $a_input);

			return $this->db->error();	      
		}

		function getLogin() {

			$_data = $this->input->post() ;

			foreach ($_data as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $this->db->where(
	    		array(
	    			'data_user.email' 	  => $a_input['email'],
	    			'data_user.password'  => $a_input['password'],
	    			'data_user.is_active' => 1
	    		)
	    	);	   

	        $query = $this->db->get($this->table_name);
				        
	        return $query->row();
	    }

	    function getUserIncharge() {
	    	
	    	$this->db->select('data_user.*, m_role.name as id_role, m_department.name as id_department');
	    	$this->db->join('m_role', 'm_role.id = data_user.id_role', 'left');  
	    	$this->db->join('m_department', 'm_department.id = data_user.id_department', 'left');  
	    	$this->db->where(
	    		array(
	    			'data_user.is_active' => '1',
	    			'data_user.id_role !=' => '1',
	    			'data_user.is_kasi' => 1,
	    		)
	    	);

	    	$this->db->or_where('data_user.is_bendahara',1);

	    	$this->db->order_by('data_user.id_department', 'ASC');

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		 function getUserBidang() {
	    	
	    	$this->db->select('data_user.id_department');
	    	$this->db->where(
	    		array(
	    			'data_user.is_active' => '1',
	    			'data_user.is_kasi' => '1',
	    		)
	    	);
	    	$this->db->group_by('data_user.id_department');

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		function updatepengaturan() {
	    	$_data = $this->input->post() ;
	    	
	        $a_input['is_kasi'] = 1;	        
	        
	        $this->db->where('id_department', $_data['id_department']);

	        $id_user = [$_data['kasie1'],$_data['kasie2'],$_data['bendahara']];
	        for ($i=0; $i < count($id_user); $i++) { 
	        	print_r($i);
	        	if($i > 1) {
	        		$a_input['is_kasi'] = 0;	        
	        		$a_input['is_bendahara'] = 1;	
	        	}
	        	$this->db->where('id', $id_user[$i]);

	        	$this->db->update($this->table_name, $a_input);
	        }        

	        return $this->db->error();	        
	    }

	    function updatepengaturanedit() {
	    	$_data = $this->input->post() ;
	    	$_userexist = $this->getByID($_data['jabatan-next']);
	    	if ($_userexist) {
	    		if($_userexist->is_kasi || $_userexist->is_kasi) {
	    			return ['code' => 100, 'message' => 'User tersebut sudah menjabat'] ;
	    		}
	    	} 
	    	
	    	$a_input['is_kasi'] = 0;
	        $a_input['is_bendahara'] = 0;

	    	$this->db->where_in('id', [$_data['jabatan-prev'],$_data['jabatan-next']]);

	        $this->db->update($this->table_name, $a_input);
	        
	        if($_data['jabatan-role'] == '1') {
	        	$a_input['is_kasi'] = 1;
	        	$a_input['is_bendahara'] = 0;
	        } else {
	        	$a_input['is_kasi'] = 0;	        
	        	$a_input['is_bendahara'] = 1;	
	        }     

	        $this->db->where('id', $_data['jabatan-next']);

	        $this->db->update($this->table_name, $a_input);

	        return $this->db->error();	   	            
	    }

	    
	}
?>